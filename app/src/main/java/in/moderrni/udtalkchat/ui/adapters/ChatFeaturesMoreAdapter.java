package in.moderrni.udtalkchat.ui.adapters;

import android.content.Context;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.constraint.ConstraintLayout;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import in.moderrni.udtalkchat.R;
import in.moderrni.udtalkchat.activities.AddUDUser;
import in.moderrni.udtalkchat.interfaces.ViewHolderClickListener;
import in.moderrni.udtalkchat.models.ChatMoreFeataresModel;
import in.moderrni.udtalkchat.ui.customeviews.CustomTextView;

public class ChatFeaturesMoreAdapter extends RecyclerView.Adapter<ChatFeaturesMoreAdapter.ViewHolder> implements View.OnClickListener {
    private static final String TAG = ChatFeaturesMoreAdapter.class.getSimpleName();
    private List<ChatMoreFeataresModel> chatMoreFeataresModels;
    private ViewHolderClickListener viewHolderClickListerner;
    private Context mcontext;

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {

        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_view_more_menu, parent, false);
        return new ViewHolder(itemView);


    }

    public void setOnItemclickListner(ViewHolderClickListener viewHolderClickListerner) {
        this.viewHolderClickListerner = viewHolderClickListerner;

    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, final int position) {
        ChatMoreFeataresModel chatMoreFeataresModel = chatMoreFeataresModels.get(position);
        holder.tvChatText.setText(chatMoreFeataresModel.getTvChatItem());
        holder.ivMoreItem.setImageResource(chatMoreFeataresModel.getIvMoreItem());
        holder.menulayout.setOnClickListener(this);

       /* holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                viewHolderClickListerner.setItemViewClick(position);
            }
        });*/

    }

    @Override
    public int getItemCount() {
        return chatMoreFeataresModels.size();
    }

    @Override
    public void onClick(View v) {

    }

    public class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        @BindView(R.id.tvChatText)
        CustomTextView tvChatText;
        @BindView(R.id.ivMoreItem)
        ImageView ivMoreItem;
        @BindView(R.id.moremenucontraint_id)
        ConstraintLayout menulayout;

        public ViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
            mcontext = itemView.getContext();
            ivMoreItem.setOnClickListener(this);

          /*  itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Log.d(TAG, "onClick: ");

                    viewHolderClickListerner.setItemViewClick(getAdapterPosition());
                }
            });*/
        }


        @Override
        public void onClick(View v) {
            viewHolderClickListerner.setItemViewClick(getAdapterPosition());
        }

    }

    public ChatFeaturesMoreAdapter(List<ChatMoreFeataresModel> moviesList, Context context) {
        this.chatMoreFeataresModels = moviesList;
        this.mcontext = context;
        notifyDataSetChanged();
    }
}