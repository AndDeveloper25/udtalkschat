package in.moderrni.udtalkchat.ui.adapters;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.constraint.ConstraintLayout;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import in.moderrni.udtalkchat.R;
import in.moderrni.udtalkchat.interfaces.ViewHolderClickListener;
import in.moderrni.udtalkchat.models.User;

public class AddMembersInGroupAdapter extends RecyclerView.Adapter<AddMembersInGroupAdapter.MyViewHolder> implements Filterable {
    Context context;
    List<User> userList;
    List<User> userListFiltered;
    private ViewHolderClickListener viewHolderClickListener;
    private boolean isChecked = false;

    public AddMembersInGroupAdapter(Context context) {
        this.context = context;
    }

    public void setList(List<User> userList) {
        this.userList = userList;
        this.userListFiltered = userList;
        notifyDataSetChanged();
    }

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.add_group_members_itemview, parent, false);
        return new MyViewHolder(view);

    }

    @Override
    public void onBindViewHolder(@NonNull MyViewHolder viewHolder, int position) {
        User user = userListFiltered.get(position);
        viewHolder.tvMobileNumber.setText(user.getMobile_no());
        viewHolder.tvUserName.setText(user.getNickname());
        viewHolder.tvUserProductDetails.setText(user.getProducts());
        viewHolder.tvCountryName.setText(user.getCountry());
        viewHolder.tvUserType.setText(user.getTypeOfUser());
        setColorAsPerUserType(viewHolder, user.getTypeOfUser().toUpperCase());
        //set selector to the itemview onSelection
        if (user.isSelected())
            viewHolder.innerCL.setBackgroundColor(context.getResources().getColor(R.color.set_selected_itemview));
        else
            viewHolder.innerCL.setBackgroundColor(context.getResources().getColor(R.color.white));
    }

    @Override
    public int getItemCount() {
        return userListFiltered.size();
    }

    @Override
    public Filter getFilter() {
        return new Filter() {
            @Override
            protected FilterResults performFiltering(CharSequence charSequence) {
                String charString = charSequence.toString();
                if (charString.isEmpty()) {
                    userListFiltered = userList;//if user not type any search filter (word) then orignal list will add to filter list
                } else { //call when user enter character into search box
                    List<User> filteredList = new ArrayList<>();
                    for (User row : userList) {

                        //in if we have added filters which we required
                        if (row.getMobile_no().contains(charString.toLowerCase()) || row.getCountry().toLowerCase().contains(charString.toLowerCase()) || row.getProducts().toLowerCase().contains(charString.toLowerCase()) || row.getTypeOfUser().toLowerCase().contains(charString.toLowerCase()) || row.getNickname().toLowerCase().contains(charString.toLowerCase())) {
                            filteredList.add(row);
                        }

                    }

                    userListFiltered = filteredList;
                }
                FilterResults filterResults = new FilterResults();
                filterResults.values = userListFiltered;
                return filterResults;

            }

            @Override
            protected void publishResults(CharSequence constraint, FilterResults results) {
                userListFiltered = (ArrayList<User>) results.values;
                notifyDataSetChanged();
            }
        };
    }

    private void setColorAsPerUserType(MyViewHolder holder, String typeOfUser) {
        switch (typeOfUser) {
            case "BUYER":
                holder.countryNameLL.setBackgroundColor(context.getResources().getColor(R.color.buyerColorBackground));
                holder.tvUserType.setTextColor(context.getResources().getColor(R.color.buyerColor));
                break;
            case "SELLER":
                holder.countryNameLL.setBackgroundColor(context.getResources().getColor(R.color.ToolBarBackground));
                holder.tvUserType.setTextColor(context.getResources().getColor(R.color.seller_text_color));
                break;
            case "AGENT":
                holder.countryNameLL.setBackgroundColor(context.getResources().getColor(R.color.active_agent));
                holder.tvUserType.setTextColor(context.getResources().getColor(R.color.agent_text_color));
                break;
            case "FRIEND":
                holder.countryNameLL.setBackgroundColor(context.getResources().getColor(R.color.active_friend));
                holder.tvUserType.setTextColor(context.getResources().getColor(R.color.friend_text_color));
                break;
        }
    }

    public void setViewHolderClickListener(ViewHolderClickListener viewHolderClickListener) {
        this.viewHolderClickListener = viewHolderClickListener;
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        //forground view
        @BindView(R.id.ivUserDp)
        ImageView ivUserDp;
        @BindView(R.id.tvCountryName)
        TextView tvCountryName;
        @BindView(R.id.tvUserType)
        TextView tvUserType;
        @BindView(R.id.tvUserName)
        TextView tvUserName;
        @BindView(R.id.tvMobileNumber)
        TextView tvMobileNumber;
        @BindView(R.id.tvUserProductDetails)
        TextView tvUserProductDetails;
        @BindView(R.id.countryNameLL)
        public LinearLayout countryNameLL;
        @BindView(R.id.viewForeground)
        public ConstraintLayout viewForeground;
        @BindView(R.id.innerCL)
        public ConstraintLayout innerCL;

        public MyViewHolder(final View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);

            //if swipe is not enabled means its grp member selection so perform long and single click on it
            itemView.setOnLongClickListener(new View.OnLongClickListener() {
                @Override
                public boolean onLongClick(View v) {
                    if (!isChecked) {
                        isChecked = true;
                        viewHolderClickListener.setItemViewClick(getAdapterPosition());
                    } else
                        itemView.setOnLongClickListener(null);
                    return false;
                }
            });
            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (isChecked)
                        viewHolderClickListener.setItemViewClick(getAdapterPosition());
                }
            });
        }
    }
}

