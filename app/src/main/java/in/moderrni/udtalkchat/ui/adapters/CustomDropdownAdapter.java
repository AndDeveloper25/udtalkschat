package in.moderrni.udtalkchat.ui.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;
import in.moderrni.udtalkchat.R;
import java.util.ArrayList;

import in.moderrni.udtalkchat.models.DropdownModel;

public class CustomDropdownAdapter extends BaseAdapter {

    private final Context mContext;
    ViewHolder holder;
    ArrayList<DropdownModel> al;
    boolean isGroup;

    @Override
    public int getCount() {
        return al.size();
    }
    public CustomDropdownAdapter(Context mContext, ArrayList<DropdownModel> al, boolean isGroup) {
        this.mContext = mContext;
        this.isGroup = isGroup;
        this.al = al;
    }

    @Override
    public Object getItem(int position) {
        return al.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        if (convertView == null) {

            holder = new ViewHolder();
            convertView = LayoutInflater.from(mContext).inflate(R.layout.dropdown_row, null);
            holder.icon = (ImageView) convertView.findViewById(R.id.icon);
            holder.title = (TextView) convertView.findViewById(R.id.title);

            convertView.setTag(holder);
        } else {
            holder = (ViewHolder) convertView.getTag();
        }
        holder.title.setText(al.get(position).getTitle());
        holder.icon.setImageResource(al.get(position).getIcon());
        return convertView;
    }

    public class ViewHolder {
        ImageView icon;
        TextView title;

    }
}
