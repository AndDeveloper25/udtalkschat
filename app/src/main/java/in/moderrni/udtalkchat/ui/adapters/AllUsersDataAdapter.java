package in.moderrni.udtalkchat.ui.adapters;


import android.app.AlertDialog;

import android.app.Dialog;

import android.app.Activity;

import android.arch.persistence.room.Delete;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.drawable.BitmapDrawable;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.RequiresApi;
import android.support.constraint.ConstraintLayout;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListPopupWindow;
import android.widget.PopupWindow;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;


import butterknife.BindView;
import butterknife.ButterKnife;
import in.moderrni.udtalkchat.R;
import in.moderrni.udtalkchat.activities.ActivityChat;
import in.moderrni.udtalkchat.activities.ActivityGroupInformation;

import in.moderrni.udtalkchat.activities.ActivityUsertypeProfileView;
import in.moderrni.udtalkchat.activities.ActivityViewNews;
import in.moderrni.udtalkchat.daimajia_swipe.RecyclerSwipeAdapter;

import in.moderrni.udtalkchat.models.User;
import in.moderrni.udtalkchat.daimajia_swipe.SwipeLayout;
import in.moderrni.udtalkchat.ui.customeviews.CustomTextView;
import in.moderrni.udtalkchat.utils.Constants;
import in.moderrni.udtalkchat.utils.core.AppSingletonDatabase;


public class AllUsersDataAdapter extends RecyclerSwipeAdapter<AllUsersDataAdapter.UserDataViewHolder> implements SwipeLayout.SwipeListener, Filterable, View.OnClickListener {

    private static final String TAG = AllUsersDataAdapter.class.getSimpleName();
    private Context context;
    private List<User> userList = new ArrayList<>();
    private List<User> userListFiltered = new ArrayList<>();
    private AppSingletonDatabase appSingletonDatabase;//singlton database object
    int adapterposition;
    int uniqueid;
    private final int PICK_CONTACT = 1;
    //dynamically add view
    ConstraintLayout arrowGroupCL, viewGroupCL, exitGroupCL;


    public AllUsersDataAdapter(Context context) {
        this.context = context;
    }

    @NonNull
    @Override
    public UserDataViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.user_information_itemview, parent, false);

        return new UserDataViewHolder(view);
    }

    public void setList(List<User> usersList) {
        this.userList = usersList;
        this.userListFiltered = usersList;
        notifyDataSetChanged();
    }

    @Override

    public void onBindViewHolder(@NonNull final UserDataViewHolder holder, int position) {
        User user = userListFiltered.get(position);
        // Glide.with(context).load(user.getIvProfilePicUrl()).into(holder.ivUserDp);
        holder.tvMobileNumber.setText(user.getMobile_no());
        holder.tvUserName.setText(user.getNickname());
        holder.tvUserProductDetails.setText(user.getProducts());
        holder.userid.setText(String.valueOf(user.getUser_id()));


        //get userid for delete/block selected row
        String id = holder.userid.getText().toString();
        uniqueid = Integer.parseInt(id);
        holder.userid.setVisibility(View.INVISIBLE);

        //set tag ti layout to get click user id
        holder.viewLL.setTag(position);
        holder.callLL.setTag(position);
        holder.deleteLL.setTag(position);
        holder.blockLL.setTag(position);
        holder.moreLL.setTag(position);
        holder.viewForeground.setTag(position);


        if (user.getProfileFlag().equalsIgnoreCase("news")) {
            adapterposition = position;
            holder.mainRightLL.setVisibility(View.GONE);
            holder.cnMainSwipeLayout.setSwipeEnabled(false);//disable swipe for News itemview
        } else if (user.getProfileFlag().equalsIgnoreCase("group")) {
            holder.countryNameLL.setVisibility(View.GONE);
            holder.tvDivider.setVisibility(View.GONE);
            holder.countryGroupLL.setVisibility(View.VISIBLE);
            holder.tvCountryGroup.setText(user.getCountry());
            /*dynamically add layout to the Group*/
            View view = LayoutInflater.from(context).inflate(R.layout.swipe_group_information_area, null);
            holder.cnMainSwipeLayout.addDrag(SwipeLayout.DragEdge.Right, view);
            arrowGroupCL = view.findViewById(R.id.arrowGroupCL);
            viewGroupCL = view.findViewById(R.id.viewGroupCL);
            exitGroupCL = view.findViewById(R.id.exitGroupCL);
           /* arrowGroupCL.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    holder.cnMainSwipeLayout.close();
                }
            });*/
            viewGroupCL.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    context.startActivity(new Intent(context, ActivityGroupInformation.class));
                }
            });
            exitGroupCL.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Toast.makeText(context, "exit ", Toast.LENGTH_SHORT).show();
                }
            });

        } else {
            holder.countryNameLL.setVisibility(View.VISIBLE);
            holder.countryGroupLL.setVisibility(View.GONE);
            holder.tvCountryName.setText(user.getCountry());
            holder.tvUserType.setText(user.getTypeOfUser().toUpperCase());
            setColorAsPerUserType(holder, user.getTypeOfUser().toUpperCase());
        }
        holder.cnMainSwipeLayout.addSwipeListener(this);//set custom listener in  main constraint layout
        holder.cnMainSwipeLayout.setShowMode(SwipeLayout.ShowMode.LayDown);//show full background layout
        mItemManger.bind(holder.itemView, position);//that will automatically close others opened swipelayout and open the current layout seamlessly.
        //holder.viewForeground.setOnClickListener(this);//add click listner to contact row
    }

    private void setColorAsPerUserType(UserDataViewHolder holder, String typeOfUser) {
        switch (typeOfUser) {
            case "BUYER":
                holder.countryNameLL.setBackgroundColor(context.getResources().getColor(R.color.buyerColorBackground));
                holder.tvUserType.setTextColor(context.getResources().getColor(R.color.buyerColor));
                break;
            case "SELLER":
                holder.countryNameLL.setBackgroundColor(context.getResources().getColor(R.color.ToolBarBackground));
                holder.tvUserType.setTextColor(context.getResources().getColor(R.color.seller_text_color));
                break;
            case "AGENT":
                holder.countryNameLL.setBackgroundColor(context.getResources().getColor(R.color.active_agent));
                holder.tvUserType.setTextColor(context.getResources().getColor(R.color.agent_text_color));
                break;
            case "FRIEND":
                holder.countryNameLL.setBackgroundColor(context.getResources().getColor(R.color.active_friend));
                holder.tvUserType.setTextColor(context.getResources().getColor(R.color.friend_text_color));
                break;
        }
    }

    @Override
    public int getItemCount() {

        if (userListFiltered == null) {
            return 0;
        }
        return userListFiltered.size();

//        return userListFiltered.size();
    }

    @Override
    public Filter getFilter() {
        return new Filter() {
            @Override
            protected FilterResults performFiltering(CharSequence charSequence) {
                String charString = charSequence.toString();
                if (charString.isEmpty()) {
                    userListFiltered = userList;
                } else {
                    List<User> filteredList = new ArrayList<>();
                    for (User row : userList) {
                        // name match condition. this might differ depending on your requirement
                        // here we are looking for name or phone number match
                        if (row.getMobile_no().contains(charString.toLowerCase()) || row.getCountry().toLowerCase().contains(charString.toLowerCase()) || row.getProducts().toLowerCase().contains(charString.toLowerCase()) || row.getTypeOfUser().toLowerCase().contains(charString.toLowerCase()) || row.getNickname().toLowerCase().contains(charString.toLowerCase())) {
                            filteredList.add(row);
                        }
                    }
                    userListFiltered = filteredList;
                }
                FilterResults filterResults = new FilterResults();
                filterResults.values = userListFiltered;
                return filterResults;

            }

            @Override
            protected void publishResults(CharSequence constraint, FilterResults results) {
                userListFiltered = (ArrayList<User>) results.values;
                notifyDataSetChanged();
            }
        };
    }

    @Override
    public int getSwipeLayoutResourceId(int position) {
        return R.id.cnMain;
    }

    @Override
    public void onClick(View v) {


        switch (v.getId()) {

        }


    }

    @Override
    public void onStartOpen(SwipeLayout layout) {
        mItemManger.closeAllExcept(layout);//at a time only single itemview will open
    }

    @Override
    public void onOpen(SwipeLayout layout) {

    }

    @Override
    public void onStartClose(SwipeLayout layout) {

    }

    @Override
    public void onClose(SwipeLayout layout) {

    }

    @Override
    public void onUpdate(SwipeLayout layout, int leftOffset, int topOffset) {

    }

    @Override
    public void onHandRelease(SwipeLayout layout, float xvel, float yvel) {

    }


    public class UserDataViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        @BindView(R.id.ivUserDp)
        ImageView ivUserDp;
        @BindView(R.id.tvCountryName)
        TextView tvCountryName;
        @BindView(R.id.tvUserType)
        TextView tvUserType;
        @BindView(R.id.tvUserName)
        TextView tvUserName;
        @BindView(R.id.tvMobileNumber)
        TextView tvMobileNumber;
        @BindView(R.id.tvCountryGroup)
        TextView tvCountryGroup;
        @BindView(R.id.tvUserProductDetails)
        TextView tvUserProductDetails;
        @BindView(R.id.viewBackground)
        public ConstraintLayout viewBackground;
        @BindView(R.id.countryNameLL)
        public LinearLayout countryNameLL;
        @BindView(R.id.countryGroupLL)
        public LinearLayout countryGroupLL;
        @BindView(R.id.viewForeground)
        public ConstraintLayout viewForeground;
        @BindView(R.id.mainRightLL)
        public LinearLayout mainRightLL;
        @BindView(R.id.tvDivider)
        public CustomTextView tvDivider;
        @BindView(R.id.tvuserid)
        public TextView userid;
        /*Background Views*/
        @BindView(R.id.viewLL)
        LinearLayout viewLL;
        @BindView(R.id.callLL)
        LinearLayout callLL;
        @BindView(R.id.deleteLL)
        LinearLayout deleteLL;
        @BindView(R.id.blockLL)
        LinearLayout blockLL;
        @BindView(R.id.moreLL)
        LinearLayout moreLL;

        /*Swipe layout view*/
        @BindView(R.id.cnMain)
        SwipeLayout cnMainSwipeLayout;


        public UserDataViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
            appSingletonDatabase = AppSingletonDatabase.getINSTANCE(context);//create instance of Room db

            //onclick on swipe layout
            viewLL.setOnClickListener(this);
            callLL.setOnClickListener(this);
            deleteLL.setOnClickListener(this);
            blockLL.setOnClickListener(this);
            moreLL.setOnClickListener(this);
            viewForeground.setOnClickListener(this);

        }


        @Override
        public void onClick(View v) {

            int pod = (int) v.getTag();
            String profileflag = userListFiltered.get(pod).getProfileFlag();
            switch (v.getId()) {

                case R.id.viewForeground:
                    if (profileflag.equals("news")) {
                       Intent intent = new Intent(context,ActivityViewNews.class);
                       context.startActivity(intent);
                    } else {
                        context.startActivity(new Intent(context, ActivityChat.class));
                    }
                    break;
                case R.id.viewLL:
                    Log.d(TAG, "onClick:  " + pod);
                    Intent intent = new Intent(context, ActivityUsertypeProfileView.class);
                    Bundle bundle = new Bundle();
                    bundle.putInt(Constants.PROFILE_ID, userListFiltered.get(pod).getUser_id());
                    intent.putExtras(bundle);
                    context.startActivity(intent);
                    break;
                case R.id.callLL:
                    MakeCall();//call from phone with alert dialoge
                    break;
                case R.id.deleteLL:
                    Toast.makeText(context, "delete get call", Toast.LENGTH_LONG).show();
                    Log.d(TAG, "onClickposition: "+pod);

                   // DeleteUser(pod);


                    break;
                case R.id.blockLL:
                    int userid = userListFiltered.get(pod).getUser_id();
                    Toast.makeText(context, "block get call", Toast.LENGTH_LONG).show();
                    //appSingletonDatabase.userDao().blockuser(userid);//block selected user(Update status to 0 in database)
                    //deleterow(pod);

                    break;
                case R.id.moreLL:

                    MoreOption(v);

                    break;
                case R.id.arrowCL:
                    cnMainSwipeLayout.close();
                    break;

            }
        }


        //Delete Row from list
        private void deleterow(int position) { //removes the row
            userListFiltered.remove(position);
            notifyItemRemoved(position);
        }

        //Make call from our mobile phone
        private void MakeCall() {
            final AlertDialog.Builder alertDialog = new AlertDialog.Builder(context);
            // Setting Dialog Message
            alertDialog.setMessage("This is not a free call … UDtalks will use your mobile network to start this call.. ");

            // Setting Positive "Continue" Button
            alertDialog.setPositiveButton("Continue", new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int which) {
                    //make a phone call
                    String phone = userListFiltered.get(getAdapterPosition()).getMobile_no();//attach phone number
                    Intent callintent = new Intent(Intent.ACTION_DIAL, Uri.fromParts("tel", phone, null));//attach call intent
                    context.startActivity(callintent);

                }
            });

            // Setting Negative "Cancel" Button
            alertDialog.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int which) {
                    //alertDialog.cancel();
                }
            });
            alertDialog.show();

        }


        //Delete User from List

        private void DeleteUser(final int id) {
            final AlertDialog.Builder alert = new AlertDialog.Builder(context);

            alert.setMessage("Do you want to Delete..");

            alert.setPositiveButton("Yes", new DialogInterface.OnClickListener() {


                @Override
                public void onClick(DialogInterface dialog, int which) {
                    //appSingletonDatabase.userDao().deleteuser(uniqueid);//delete selected user(Update status to 0 in database)

                    deleterow(id);
                    Log.d(TAG, "onClick: "+id);
                }
            });

            alert.setNegativeButton("No", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    //close dialoge
                }
            });

            alert.show();
        }

        //Call more option
        private void MoreOption(View v) {

            // inflate your layout or dynamically add view
            LayoutInflater inflater = LayoutInflater.from(context);

            View view = inflater.inflate(R.layout.list_moreoption, null);

            view.measure(View.MeasureSpec.UNSPECIFIED, View.MeasureSpec.UNSPECIFIED);
            final PopupWindow popupDialog = new PopupWindow(view, view.getMeasuredHeight(), view.getMeasuredWidth(), true);


            popupDialog.setFocusable(true);
            popupDialog.setWidth(650);
            popupDialog.setHeight(ListPopupWindow.WRAP_CONTENT);
            popupDialog.setOutsideTouchable(true);
            popupDialog.setTouchable(true);
            popupDialog.setContentView(view);
            popupDialog.setBackgroundDrawable(new BitmapDrawable());
            popupDialog.setOutsideTouchable(true);
            popupDialog.showAsDropDown(v, 40, 10);


            final ConstraintLayout constraintLayoutstickontop = (ConstraintLayout) view.findViewById(R.id.contrainLstickontop_id);
            final ConstraintLayout constraintLayoutsortabove = (ConstraintLayout) view.findViewById(R.id.contraintLshortabove_id);
            final ConstraintLayout constraintLayoutmarkasunread = (ConstraintLayout) view.findViewById(R.id.contraintLmarkasunread_id);
            final ConstraintLayout constraintLayoutaddtodesktop = (ConstraintLayout) view.findViewById(R.id.contraintLaddtodesktop_id);
            final ConstraintLayout constraintLayoutcancel = (ConstraintLayout) view.findViewById(R.id.contrainLCancel_id);


            //onclicks on Layout
            //Stick on top Click
            constraintLayoutstickontop.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    constraintLayoutstickontop.setBackgroundColor(context.getResources().getColor(R.color.moreoptionselectbagroung));
                    constraintLayoutsortabove.setBackgroundColor(context.getResources().getColor(R.color.defaultbaground));
                    constraintLayoutaddtodesktop.setBackgroundColor(context.getResources().getColor(R.color.defaultbaground));
                    constraintLayoutmarkasunread.setBackgroundColor(context.getResources().getColor(R.color.defaultbaground));

                }
            });

            constraintLayoutsortabove.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    constraintLayoutsortabove.setBackgroundColor(context.getResources().getColor(R.color.moreoptionselectbagroung));
                    constraintLayoutaddtodesktop.setBackgroundColor(context.getResources().getColor(R.color.defaultbaground));
                    constraintLayoutmarkasunread.setBackgroundColor(context.getResources().getColor(R.color.defaultbaground));
                    constraintLayoutstickontop.setBackgroundColor(context.getResources().getColor(R.color.defaultbaground));
                }
            });

            constraintLayoutmarkasunread.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    constraintLayoutmarkasunread.setBackgroundColor(context.getResources().getColor(R.color.moreoptionselectbagroung));
                    constraintLayoutsortabove.setBackgroundColor(context.getResources().getColor(R.color.defaultbaground));
                    constraintLayoutaddtodesktop.setBackgroundColor(context.getResources().getColor(R.color.defaultbaground));
                    constraintLayoutstickontop.setBackgroundColor(context.getResources().getColor(R.color.defaultbaground));
                }
            });

            constraintLayoutaddtodesktop.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    constraintLayoutaddtodesktop.setBackgroundColor(context.getResources().getColor(R.color.moreoptionselectbagroung));
                    constraintLayoutmarkasunread.setBackgroundColor(context.getResources().getColor(R.color.defaultbaground));
                    constraintLayoutsortabove.setBackgroundColor(context.getResources().getColor(R.color.defaultbaground));
                    constraintLayoutstickontop.setBackgroundColor(context.getResources().getColor(R.color.defaultbaground));
                }
            });
            constraintLayoutcancel.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    popupDialog.dismiss();

                }
            });

        }
    }


}
