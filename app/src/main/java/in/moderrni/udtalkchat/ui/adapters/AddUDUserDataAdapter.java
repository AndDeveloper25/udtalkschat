package in.moderrni.udtalkchat.ui.adapters;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.constraint.ConstraintLayout;
import android.support.v7.widget.LinearSmoothScroller;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import in.moderrni.udtalkchat.R;
import in.moderrni.udtalkchat.activities.ActivityUserView;
import in.moderrni.udtalkchat.activities.ActivityUsertypeProfileView;
import in.moderrni.udtalkchat.daimajia_swipe.RecyclerSwipeAdapter;
import in.moderrni.udtalkchat.daimajia_swipe.SwipeLayout;
import in.moderrni.udtalkchat.interfaces.ViewHolderClickListener;
import in.moderrni.udtalkchat.models.User;
import in.moderrni.udtalkchat.utils.Constants;

public class AddUDUserDataAdapter extends RecyclerSwipeAdapter<AddUDUserDataAdapter.UDUserViewHolder> implements SwipeLayout.SwipeListener, Filterable {

    Context context;
    private List<User> userlist = new ArrayList<>();
    private List<User> userListFiltered = new ArrayList<>();

    public AddUDUserDataAdapter(Context mContext) {
        this.context = mContext;
    }

    public void SetList(List<User> userlist) {
        this.userlist = userlist;
        this.userListFiltered = userlist;
        notifyDatasetChanged();

    }

    @NonNull
    @Override
    public UDUserViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.swiped_added_user_not_yet, parent, false);
        return new AddUDUserDataAdapter.UDUserViewHolder(view);
    }


    @Override
    public void onBindViewHolder(@NonNull UDUserViewHolder viewHolder, int position) {
        User user = userListFiltered.get(position);
        viewHolder.tvMobileNumber.setText(user.getMobile_no());
        viewHolder.tvUserName.setText(user.getNickname());
        viewHolder.tvUserProductDetails.setText(user.getProducts());
        viewHolder.tvCountryName.setText(user.getCountry());
        viewHolder.tvUserType.setText(user.getTypeOfUser());
        setColorAsPerUserType(viewHolder, user.getTypeOfUser().toUpperCase());
        viewHolder.cnMainSwipeLayout.setSwipeEnabled(true);//if add from UD users then swipe enabled
        viewHolder.cnMainSwipeLayout.addSwipeListener(this);//set custom listener in  main constraint layout
        viewHolder.cnMainSwipeLayout.setShowMode(SwipeLayout.ShowMode.LayDown);//show full background layout
        mItemManger.bind(viewHolder.itemView, position);//that will automatically close others opened swipelayout and open the current layout seamlessly.

    }


    @Override
    public int getItemCount() {
        return userListFiltered.size();
    }

    private void setColorAsPerUserType(UDUserViewHolder holder, String typeOfUser) {
        switch (typeOfUser) {
            case "BUYER":
                holder.countryNameLL.setBackgroundColor(context.getResources().getColor(R.color.buyerColorBackground));
                holder.tvUserType.setTextColor(context.getResources().getColor(R.color.buyerColor));
                break;
            case "SELLER":
                holder.countryNameLL.setBackgroundColor(context.getResources().getColor(R.color.ToolBarBackground));
                holder.tvUserType.setTextColor(context.getResources().getColor(R.color.seller_text_color));
                break;
            case "AGENT":
                holder.countryNameLL.setBackgroundColor(context.getResources().getColor(R.color.active_agent));
                holder.tvUserType.setTextColor(context.getResources().getColor(R.color.agent_text_color));
                break;
            case "FRIEND":
                holder.countryNameLL.setBackgroundColor(context.getResources().getColor(R.color.active_friend));
                holder.tvUserType.setTextColor(context.getResources().getColor(R.color.friend_text_color));
                break;
        }
    }

    @Override
    public Filter getFilter() {
        return new Filter() {
            @Override
            protected FilterResults performFiltering(CharSequence charSequence) {
                String charString = charSequence.toString();
                if (charString.isEmpty()) {
                    userListFiltered = userlist;//if user not type any search filter (word) then orignal list will add to filter list
                } else { //call when user enter character into search box
                    List<User> filteredList = new ArrayList<>();
                    for (User row : userlist) {

                        //in if we have added filters which we required
                        if (row.getMobile_no().contains(charString.toLowerCase()) || row.getCountry().toLowerCase().contains(charString.toLowerCase()) || row.getProducts().toLowerCase().contains(charString.toLowerCase()) || row.getTypeOfUser().toLowerCase().contains(charString.toLowerCase()) || row.getNickname().toLowerCase().contains(charString.toLowerCase())) {
                            filteredList.add(row);
                        }

                    }

                    userListFiltered = filteredList;
                }
                FilterResults filterResults = new FilterResults();
                filterResults.values = userListFiltered;
                return filterResults;

            }

            @Override
            protected void publishResults(CharSequence constraint, FilterResults results) {
                userListFiltered = (ArrayList<User>) results.values;
                notifyDataSetChanged();
            }
        };
    }

    @Override
    public void onStartOpen(SwipeLayout layout) {
        mItemManger.closeAllExcept(layout);//at a time only single itemview will open
    }

    @Override
    public void onOpen(SwipeLayout layout) {

    }

    @Override
    public void onStartClose(SwipeLayout layout) {

    }

    @Override
    public void onClose(SwipeLayout layout) {

    }

    @Override
    public void onUpdate(SwipeLayout layout, int leftOffset, int topOffset) {

    }

    @Override
    public void onHandRelease(SwipeLayout layout, float xvel, float yvel) {

    }

    @Override
    public int getSwipeLayoutResourceId(int position) {
        return R.id.cnMain;
    }

    //Class for Hold view
    public class UDUserViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        //forground view
        @BindView(R.id.ivUserDp)
        ImageView ivUserDp;
        @BindView(R.id.tvCountryName)
        TextView tvCountryName;
        @BindView(R.id.tvUserType)
        TextView tvUserType;
        @BindView(R.id.tvUserName)
        TextView tvUserName;
        @BindView(R.id.tvMobileNumber)
        TextView tvMobileNumber;
        @BindView(R.id.tvUserProductDetails)
        TextView tvUserProductDetails;
        @BindView(R.id.viewBackground)
        public ConstraintLayout viewBackground;
        @BindView(R.id.countryNameLL)
        public LinearLayout countryNameLL;
        @BindView(R.id.viewForeground)
        public ConstraintLayout viewForeground;

        //baground view

        @BindView(R.id.ll_SwipedView)
        LinearLayout llViewprofile;
        @BindView(R.id.ll_Swipedadd)
        LinearLayout lladdprofile;
        @BindView(R.id.arrowGroupCL)
        ConstraintLayout arrowCL;

        /*Swipe layout view*/
        @BindView(R.id.cnMain)
        SwipeLayout cnMainSwipeLayout;


        public UDUserViewHolder(final View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
            llViewprofile.setOnClickListener(this);
            lladdprofile.setOnClickListener(this);
            arrowCL.setOnClickListener(this);

        }

        @Override
        public void onClick(View v) {
            switch (v.getId()) {
                case (R.id.ll_SwipedView):
                    Intent intent = new Intent(context, ActivityUsertypeProfileView.class);
                    Bundle bundle = new Bundle();
                    bundle.putInt(Constants.PROFILE_ID, userListFiltered.get(getAdapterPosition()).getUser_id());
                    intent.putExtras(bundle);
                    context.startActivity(intent);
                    break;

                case (R.id.ll_Swipedadd):
                    Toast.makeText(context, "add get click", Toast.LENGTH_LONG).show();
                    break;
            }
        }


    }


}
