package in.moderrni.udtalkchat.activities;

import android.os.Bundle;
import android.os.StrictMode;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.widget.TextView;


import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import in.moderrni.udtalkchat.R;
import in.moderrni.udtalkchat.fragments.ChatFragment;
import in.moderrni.udtalkchat.fragments.ManageFragment;
import in.moderrni.udtalkchat.utils.Constants;
import in.moderrni.udtalkchat.utils.Utility;

public class HomeActivity extends AppCompatActivity {
    @BindView(R.id.tv_chatTab)
    TextView tv_chatTab;
    @BindView(R.id.tv_dealsTab)
    TextView tv_dealsTab;
    @BindView(R.id.tv_scanTab)
    TextView tv_scanTab;
    @BindView(R.id.tv_manageTab)
    TextView tv_manageTab;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);
        ButterKnife.bind(this);
        StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
        StrictMode.setThreadPolicy(policy);
        Utility.setSharedPreference(this, Constants.IS_LOGIN, Constants.IS_LOGIN);
        initFrag(new ChatFragment());
//        uploadFile();

    }




    public void initFrag(Fragment fragment) {
//        Fragment fragment=fragments.get(position);
        String fragClassName = fragment.getClass().getSimpleName();
//        Toast.makeText(MainActivity.this,backStateName,Toast.LENGTH_LONG).show();
        String fragmentTag = fragClassName;


        FragmentManager manager = getSupportFragmentManager();
        boolean fragmentPopped = manager.popBackStackImmediate(fragClassName, 0);
//
        if (!fragmentPopped && manager.findFragmentByTag(fragmentTag) == null) { //fragment not in back stack, create it.
            FragmentTransaction ft = manager.beginTransaction();
            ft.replace(R.id.mainFrameLayout, fragment, fragmentTag);
            ft.addToBackStack(fragClassName);
            ft.commit();
        }
    }


    @OnClick(R.id.tv_chatTab)
    void setChatActiveColor() {
        initFrag(new ChatFragment());
        tv_chatTab.setTextColor(getApplicationContext().getResources().getColor(R.color.ud_blue));
        tv_dealsTab.setTextColor(getApplicationContext().getResources().getColor(R.color.inactive_text_157));
        tv_scanTab.setTextColor(getApplicationContext().getResources().getColor(R.color.inactive_text_157));
        tv_manageTab.setTextColor(getApplicationContext().getResources().getColor(R.color.inactive_text_157));

        tv_chatTab.setBackgroundColor(getApplicationContext().getResources().getColor(R.color.active_tabs));
        tv_dealsTab.setBackgroundColor(getApplicationContext().getResources().getColor(R.color.inactive_tabs));
        tv_scanTab.setBackgroundColor(getApplicationContext().getResources().getColor(R.color.inactive_tabs));
        tv_manageTab.setBackgroundColor(getApplicationContext().getResources().getColor(R.color.inactive_tabs));
    }

    @OnClick(R.id.tv_dealsTab)
    void setDealsActiveColor() {
        tv_dealsTab.setTextColor(getResources().getColor(R.color.ud_blue));
        tv_chatTab.setTextColor(getResources().getColor(R.color.inactive_text_157));
        tv_scanTab.setTextColor(getResources().getColor(R.color.inactive_text_157));
        tv_manageTab.setTextColor(getResources().getColor(R.color.inactive_text_157));

        tv_dealsTab.setBackgroundColor(getResources().getColor(R.color.active_tabs));
        tv_chatTab.setBackgroundColor(getResources().getColor(R.color.inactive_tabs));
        tv_scanTab.setBackgroundColor(getResources().getColor(R.color.inactive_tabs));
        tv_manageTab.setBackgroundColor(getResources().getColor(R.color.inactive_tabs));

    }

    @OnClick(R.id.tv_scanTab)
    void setScanActiveColor() {
        tv_scanTab.setTextColor(getApplicationContext().getResources().getColor(R.color.ud_blue));
        tv_chatTab.setTextColor(getApplicationContext().getResources().getColor(R.color.inactive_text_157));
        tv_dealsTab.setTextColor(getApplicationContext().getResources().getColor(R.color.inactive_text_157));
        tv_manageTab.setTextColor(getApplicationContext().getResources().getColor(R.color.inactive_text_157));

        tv_scanTab.setBackgroundColor(getApplicationContext().getResources().getColor(R.color.active_tabs));
        tv_chatTab.setBackgroundColor(getApplicationContext().getResources().getColor(R.color.inactive_tabs));
        tv_dealsTab.setBackgroundColor(getApplicationContext().getResources().getColor(R.color.inactive_tabs));
        tv_manageTab.setBackgroundColor(getApplicationContext().getResources().getColor(R.color.inactive_tabs));
    }

    @OnClick(R.id.tv_manageTab)
    void setManageActiveColor() {
        initFrag(new ManageFragment());
        tv_manageTab.setTextColor(getApplicationContext().getResources().getColor(R.color.ud_blue));
        tv_chatTab.setTextColor(getApplicationContext().getResources().getColor(R.color.inactive_text_157));
        tv_dealsTab.setTextColor(getApplicationContext().getResources().getColor(R.color.inactive_text_157));
        tv_scanTab.setTextColor(getApplicationContext().getResources().getColor(R.color.inactive_text_157));

        tv_manageTab.setBackgroundColor(getApplicationContext().getResources().getColor(R.color.active_tabs));
        tv_chatTab.setBackgroundColor(getApplicationContext().getResources().getColor(R.color.inactive_tabs));
        tv_dealsTab.setBackgroundColor(getApplicationContext().getResources().getColor(R.color.inactive_tabs));
        tv_scanTab.setBackgroundColor(getApplicationContext().getResources().getColor(R.color.inactive_tabs));
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        Fragment currentFragment = getSupportFragmentManager()
                .findFragmentById(R.id.mainFrameLayout);
        if (currentFragment != null) {
            if (currentFragment.getClass().getSimpleName().equals("ChatListFragment")) {
                setChatActiveColor();
            } else if (currentFragment.getClass().getSimpleName().equals("ManageFragment")) {
                setManageActiveColor();
            } else if (currentFragment.getClass().getSimpleName().equals("DealsFragment")) {
                setDealsActiveColor();
            }

        } else {
            /*exit app when stack of fragment is empty*/
            HomeActivity.this.finish();
        }
    }
}
