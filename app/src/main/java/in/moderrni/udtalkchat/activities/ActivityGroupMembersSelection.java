package in.moderrni.udtalkchat.activities;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.constraint.ConstraintLayout;

import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;

import android.widget.ImageView;
import android.widget.Toast;


import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import de.hdodenhof.circleimageview.CircleImageView;
import in.moderrni.udtalkchat.R;
import in.moderrni.udtalkchat.interfaces.ViewHolderClickListener;
import in.moderrni.udtalkchat.models.Group;

import in.moderrni.udtalkchat.models.ModelStandardResponse;
import in.moderrni.udtalkchat.models.User;
import in.moderrni.udtalkchat.ui.adapters.AddMembersInGroupAdapter;

import in.moderrni.udtalkchat.ui.customeviews.CustomButtonView;
import in.moderrni.udtalkchat.ui.customeviews.CustomEditText;
import in.moderrni.udtalkchat.ui.customeviews.CustomTextView;
import in.moderrni.udtalkchat.utils.SharedPref;
import in.moderrni.udtalkchat.utils.core.AppSingletonDatabase;
import io.reactivex.Observable;
import io.reactivex.Observer;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;

public class ActivityGroupMembersSelection extends BaseActivity implements ViewHolderClickListener, View.OnClickListener {
    @BindView(R.id.imgGroupIcon)
    CircleImageView imgGroupIcon;
    @BindView(R.id.btnCreate)
    CustomButtonView btnCreate;
    @BindView(R.id.txtMemberDetails)
    CustomTextView txtMemberDetails;
    @BindView(R.id.recyclerView)
    RecyclerView recyclerView;
    @BindView(R.id.ivAdduser)
    ImageView ivAdduser;
    @BindView(R.id.ivSearch)
    ImageView ivSearch;
    @BindView(R.id.cnSearchBar)
    ConstraintLayout cnSearchBar;
    @BindView(R.id.etSearchBar)
    CustomEditText etSearchBar;
    private LinearLayoutManager linearLayoutManager;
    private AddMembersInGroupAdapter addMembersInGroupAdapter;
    private AppSingletonDatabase appSingletonDatabase;
    private List<User> userList;
    private int selectedCount = 0;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_select_group_members);
        ButterKnife.bind(this);
        initView();
    }

    private void initView() {
        setListener();
        txtMemberDetails.setText(getString(R.string.multiple_members_selected, selectedCount));//set default value to member seletction
        appSingletonDatabase = AppSingletonDatabase.getINSTANCE(this);//create instance of Room db
        userList = new ArrayList<>();
        linearLayoutManager = new LinearLayoutManager(this);
        recyclerView.setLayoutManager(linearLayoutManager);//set layout to the recyclerview
        addMembersInGroupAdapter = new AddMembersInGroupAdapter(this);
        addMembersInGroupAdapter.setViewHolderClickListener(this);
        recyclerView.setAdapter(addMembersInGroupAdapter);//set adapter to the recyclerview
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        recyclerView.addItemDecoration(new DividerItemDecoration(this, DividerItemDecoration.VERTICAL));
    }

    private void setListener() {
        ivSearch.setOnClickListener(this);
        btnCreate.setOnClickListener(this);
        ivAdduser.setVisibility(View.GONE);
        etSearchBar.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                addMembersInGroupAdapter.getFilter().filter(s);//set filter on edittext to search in list
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });

    }

    @Override
    protected void onStart() {
        super.onStart();
        if (userList.size() > 0) {
            userList.clear();
            getListFromRoom();
            Toast.makeText(this, "if ", Toast.LENGTH_SHORT).show();
        } else {
            getListFromRoom();
            Toast.makeText(this, "else ", Toast.LENGTH_SHORT).show();
        }
    }

    private void getListFromRoom() {
        userList = appSingletonDatabase.userDao().getOnlyContacts(1, "user");
        addMembersInGroupAdapter.setList(userList);
        addMembersInGroupAdapter.notifyDataSetChanged();
    }

    @Override
    public void setItemViewClick(int position) {
        //if User is not selected then select it by setting true and if already selected then make it false;
        if (userList.get(position).isSelected()) {
            userList.get(position).setSelected(false);
        } else
            userList.get(position).setSelected(true);
        selectedCount = getSelectedMemberCount();
        if (selectedCount != 1)
            txtMemberDetails.setText(getString(R.string.multiple_members_selected, selectedCount));
        else
            txtMemberDetails.setText(getString(R.string.single_member_selected, selectedCount));
        addMembersInGroupAdapter.notifyDataSetChanged();

    }

    private int getSelectedMemberCount() {
        int count = 0;
        for (User user : userList) {
            if (user.isSelected())
                count++;
        }
        return count;
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.ivSearch:
                cnSearchBar.setVisibility(View.VISIBLE);
                break;
            case R.id.btnCreate:
                createObservalbleOgGroup();
                break;


        }
    }

    private void createObservalbleOgGroup() {
        Observable<ModelStandardResponse> modelStandardResponseObservable = Observable.just(createUser());
        modelStandardResponseObservable.subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Observer<ModelStandardResponse>() {
                    @Override
                    public void onSubscribe(Disposable d) {

                    }

                    @Override
                    public void onNext(ModelStandardResponse modelStandardResponse) {
                        if (modelStandardResponse.getStatus()) {
                            setListToRoom(modelStandardResponse);
                        }
                    }

                    @Override
                    public void onError(Throwable e) {

                    }

                    @Override
                    public void onComplete() {

                    }
                });
    }

    private void setListToRoom(ModelStandardResponse modelStandardResponse) {
        //get all group list of user
        List<Group> groupList = new ArrayList<>();
        //List<Group> groupList = appSingletonDatabase.userDao().getAllGroupList(Integer.parseInt(SharedPref.getUserid(getApplicationContext())));
        Group group = new Group(modelStandardResponse.getGroupId(), "TEST GRP NAME", "XMPP GRP NAME", "GRP PIC", "1", "GRP DESCRIPTION", getSelectedUsersList());
        groupList.add(group);
        //appSingletonDatabase.userDao().insertGroupList(Integer.parseInt(SharedPref.getUserid(getApplicationContext())),groupList);
    }

    private List<User> getSelectedUsersList() {
        List<User> getSelectedUserList = new ArrayList<>();
        for (User user : userList) {
            if (user.isSelected()) {
                getSelectedUserList.add(user);
            }
        }
        return getSelectedUserList;
    }

    //call create group api here
    private ModelStandardResponse createUser() {
        return new ModelStandardResponse(true, 1, "", "Group created successfully");
    }
}
