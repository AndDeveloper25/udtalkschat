
package in.moderrni.udtalkchat.activities;

import android.os.Build;
import android.os.Handler;
import android.support.annotation.RequiresApi;
import android.support.v4.view.ViewPager;
import android.os.Bundle;
import android.view.View;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import in.moderrni.udtalkchat.R;
import butterknife.BindView;
import butterknife.ButterKnife;
import in.moderrni.udtalkchat.fragments.ChatFragment;
import in.moderrni.udtalkchat.fragments.DealFragment;
import in.moderrni.udtalkchat.fragments.ManageFragment;
import in.moderrni.udtalkchat.fragments.ScanFragment;
import in.moderrni.udtalkchat.ui.adapters.MainViewPagerAdapter;
import in.moderrni.udtalkchat.ui.customeviews.CustomViewPager;

public class MainActivity extends BaseActivity implements View.OnClickListener {
    @BindView(R.id.viewPager)
    CustomViewPager viewPager;
    @BindView(R.id.txtChat)
    TextView txtChat;
    @BindView(R.id.txtDeals)
    TextView txtDeals;
    @BindView(R.id.txtScan)
    TextView txtScan;
    @BindView(R.id.txtManage)
    TextView txtManage;
    @BindView(R.id.rlChat)
    RelativeLayout rlChat;
    @BindView(R.id.rlDeals)
    RelativeLayout rlDeals;
    @BindView(R.id.rlScan)
    RelativeLayout rlScan;
    @BindView(R.id.rlManage)
    RelativeLayout rlManage;
    boolean doubleBackToExitPressedOnce = false;
//    @BindView(R.id.cnSearchBar)
//    ConstraintLayout cnSearchBar;
//    @BindView(R.id.ivSearch)
//    ImageView ivSearch;
//    @BindView(R.id.ivAdduser)
//    ImageView ivAdduser;
//    @BindView(R.id.ivCancalSearchBar)
//    ImageView ivCancalSearchBar;
//
//    @BindView(R.id.toolbar)
//    android.support.constraint.ConstraintLayout toolbar;
//    //
//    private PopupWindow popupDialog;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        initView();


    }
    private void initView() {
        ButterKnife.bind(this);
        rlChat.setOnClickListener(this);
        rlDeals.setOnClickListener(this);
        rlScan.setOnClickListener(this);
        rlManage.setOnClickListener(this);
//        cnSearchBar.setOnClickListener(this);
//        ivSearch.setOnClickListener(this);
//        ivAdduser.setOnClickListener(this);
//        ivCancalSearchBar.setOnClickListener(this);
        setupViewPager();
        viewPager.setPagingEnabled(false);//disable the swiping functionality of ViewPager
        setListeners();
    }
    private void setListeners() {
        viewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }

            @Override
            public void onPageSelected(int position) {
                switch (position) {
                    case 0:
                        setDynamicBk("chat");
                        break;

                    case 1:
                        setDynamicBk("deals");
                        break;

                    case 2:
                        setDynamicBk("scan");
                        break;

                    case 3:
                        setDynamicBk("manage");
                        break;
                }
            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });
    }
    @Override
    public void onBackPressed() {
        if (doubleBackToExitPressedOnce) {
            super.onBackPressed();
            return;
        }

        this.doubleBackToExitPressedOnce = true;
        Toast.makeText(this, "Back again to exit", Toast.LENGTH_SHORT).show();

        new Handler().postDelayed(new Runnable() {

            @Override
            public void run() {
                doubleBackToExitPressedOnce = false;
            }
        }, 2000);
    }
    private void setupViewPager() {
        MainViewPagerAdapter mainViewPagerAdapter = new MainViewPagerAdapter(getSupportFragmentManager());
        mainViewPagerAdapter.addFragment(new ChatFragment());
        mainViewPagerAdapter.addFragment(new DealFragment());
        mainViewPagerAdapter.addFragment(new ScanFragment());
        mainViewPagerAdapter.addFragment(new ManageFragment());
        viewPager.setAdapter(mainViewPagerAdapter);
    }

    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.rlChat:
                viewPager.setCurrentItem(0);
                setDynamicBk("chat");
                break;
            case R.id.rlDeals:
                viewPager.setCurrentItem(1);
                setDynamicBk("deals");
                break;
            case R.id.rlScan:
                viewPager.setCurrentItem(2);
                setDynamicBk("scan");
                break;
            case R.id.rlManage:
                viewPager.setCurrentItem(3);
                setDynamicBk("manage");
                break;
//            case R.id.cnSearchBar:
//                break;
//            case R.id.ivSearch:
//                cnSearchBar.setVisibility(View.VISIBLE);
//                break;
//            case R.id.ivAdduser:
//                OpenUserAddDialoge();
//                Toast.makeText(this, "add user", Toast.LENGTH_SHORT).show();
//                break;
//            case R.id.ivCancalSearchBar:
//                cnSearchBar.setVisibility(View.GONE);
//                break;
        }

    }

    private void setDynamicBk(String strValue) {
        switch (strValue) {
            case "chat":
                rlChat.setBackgroundColor(getResources().getColor(R.color.bottomViewSelectedColor));
                txtChat.setTextColor(getResources().getColor(R.color.bottomTvSelectedColor));

                rlDeals.setBackgroundColor(getResources().getColor(R.color.bottomBarColor));
                txtDeals.setTextColor(getResources().getColor(R.color.bottomBarTxtColor));

                rlScan.setBackgroundColor(getResources().getColor(R.color.bottomBarColor));
                txtScan.setTextColor(getResources().getColor(R.color.bottomBarTxtColor));

                rlManage.setBackgroundColor(getResources().getColor(R.color.bottomBarColor));
                txtManage.setTextColor(getResources().getColor(R.color.bottomBarTxtColor));
                break;
            case "deals":
                rlDeals.setBackgroundColor(getResources().getColor(R.color.bottomViewSelectedColor));
                txtDeals.setTextColor(getResources().getColor(R.color.bottomTvSelectedColor));

                rlChat.setBackgroundColor(getResources().getColor(R.color.bottomBarColor));
                txtChat.setTextColor(getResources().getColor(R.color.bottomBarTxtColor));

                rlScan.setBackgroundColor(getResources().getColor(R.color.bottomBarColor));
                txtScan.setTextColor(getResources().getColor(R.color.bottomBarTxtColor));

                rlManage.setBackgroundColor(getResources().getColor(R.color.bottomBarColor));
                txtManage.setTextColor(getResources().getColor(R.color.bottomBarTxtColor));
                break;
            case "scan":
                rlScan.setBackgroundColor(getResources().getColor(R.color.bottomViewSelectedColor));
                txtScan.setTextColor(getResources().getColor(R.color.bottomTvSelectedColor));

                rlChat.setBackgroundColor(getResources().getColor(R.color.bottomBarColor));
                txtChat.setTextColor(getResources().getColor(R.color.bottomBarTxtColor));

                rlDeals.setBackgroundColor(getResources().getColor(R.color.bottomBarColor));
                txtDeals.setTextColor(getResources().getColor(R.color.bottomBarTxtColor));

                rlManage.setBackgroundColor(getResources().getColor(R.color.bottomBarColor));
                txtManage.setTextColor(getResources().getColor(R.color.bottomBarTxtColor));
                break;
            case "manage":
                rlManage.setBackgroundColor(getResources().getColor(R.color.bottomViewSelectedColor));
                txtManage.setTextColor(getResources().getColor(R.color.bottomTvSelectedColor));

                rlChat.setBackgroundColor(getResources().getColor(R.color.bottomBarColor));
                txtChat.setTextColor(getResources().getColor(R.color.bottomBarTxtColor));

                rlDeals.setBackgroundColor(getResources().getColor(R.color.bottomBarColor));
                txtDeals.setTextColor(getResources().getColor(R.color.bottomBarTxtColor));

                rlScan.setBackgroundColor(getResources().getColor(R.color.bottomBarColor));
                txtScan.setTextColor(getResources().getColor(R.color.bottomBarTxtColor));
                break;
        }


    }
/*
    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    private void OpenUserAddDialoge() {
//        android.support.constraint.ConstraintLayout viewGroup = (android.support.constraint.ConstraintLayout)findViewById(R.id.mainactivitylayout_id) ;
        LayoutInflater inflater = LayoutInflater.from(this);
        View layout = inflater.inflate(R.layout.layout_dropdown_adduser, null);
        popupDialog = new PopupWindow();
        popupDialog.setContentView(layout);
        popupDialog.setWidth(WindowManager.LayoutParams.MATCH_PARENT);
        popupDialog.setHeight(WindowManager.LayoutParams.MATCH_PARENT);
        popupDialog.setFocusable(true);
        popupDialog.setBackgroundDrawable(null);//for removing borders and margins
        popupDialog.showAsDropDown(toolbar);
//        popupDialog.setAnimationStyle(R.style.Animation);
        ListView listPopup1 = (ListView) layout.findViewById(R.id.listPopup);
        ArrayList<DropdownModel> al = new ArrayList<>();
        al.add(new DropdownModel(R.drawable.ic_usergroup, getResources().getString(R.string.create_business_group)));
        al.add(new DropdownModel(R.drawable.ic_group, getResources().getString(R.string.add_ud_users)));
        al.add(new DropdownModel(R.drawable.ic_phonebook, getResources().getString(R.string.add_phone_contacts)));
        al.add(new DropdownModel(R.drawable.ic_scanner, getResources().getString(R.string.scan_to_add_user)));
        View popup_background = layout.findViewById(R.id.popup_background);
        popup_background.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                popupDialog.dismiss();
            }
        });
        CustomDropdownAdapter adapter = new CustomDropdownAdapter(this, al, false);
        listPopup1.setAdapter(adapter);
        listPopup1.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                switch (position) {
                    case 0:
                        Intent intent = new Intent(MainActivity.this,ActivityCreateBusinessGroup.class);
                        startActivity(intent);
                        Toast.makeText(MainActivity.this, "create_business_group", Toast.LENGTH_SHORT).show();
                        break;
                    case 1:

                        startActivity(new Intent(MainActivity.this, AddUDUser.class));

                        //  startActivity(new Intent(getActivity(), AddUdUserActivity.class));
                        Toast.makeText(MainActivity.this, "add_ud_users", Toast.LENGTH_SHORT).show();

                        break;
                    case 2:
                        Toast.makeText(MainActivity.this, "add_phone_contacts", Toast.LENGTH_SHORT).show();

//
                        break;
                    case 3:
//
                        Toast.makeText(MainActivity.this, "scan_to_add_user", Toast.LENGTH_SHORT).show();
                        break;
                }
                popupDialog.dismiss();
            }

        });
        popupDialog.setOnDismissListener(new PopupWindow.OnDismissListener() {
            @Override
            public void onDismiss() {
                // iv_add_user.setImageResource(R.drawable.ic_add_dots);
            }
        });


    }*/

}

