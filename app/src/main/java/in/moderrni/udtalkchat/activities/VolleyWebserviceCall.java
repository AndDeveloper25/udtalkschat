package in.moderrni.udtalkchat.activities;

import android.app.Dialog;
import android.content.Context;

import com.android.volley.AuthFailureError;
import com.android.volley.NetworkError;
import com.android.volley.NetworkResponse;
import com.android.volley.NoConnectionError;
import com.android.volley.ParseError;
import com.android.volley.Request.Method;
import com.android.volley.Response;
import com.android.volley.RetryPolicy;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.StringRequest;

import org.json.JSONException;
import org.json.JSONObject;

import in.moderrni.udtalkchat.interfaces.VolleyWebserviceCallBack;
import in.moderrni.udtalkchat.utils.Dialogs;
import in.moderrni.udtalkchat.utils.UDTalkApplication;


/**
 * Created by suheb on 24/5/17.
 */

public class VolleyWebserviceCall {
    private static VolleyWebserviceCall instance;
    private int requestTimeout = 50000;
    private int statusCode = 400;
    Dialog dialog = null;

    private VolleyWebserviceCall() {
    }

    public synchronized static VolleyWebserviceCall getInstance() {
        if (instance == null) {
            if (instance == null)
                instance = new VolleyWebserviceCall();
        }
        return instance;
    }


    public void postApiCall(final Context context, final String url, final JSONObject jObj, final VolleyWebserviceCallBack callBack, final Boolean showProgressDialog, final String tag) {

        if (showProgressDialog) {
            dialog = Dialogs.getInstance().showCircleLoading(context);
            dialog.show();
        }
        final JsonObjectRequest request = new JsonObjectRequest(Method.POST, url, jObj, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject jsonObject) {
                try {
                    if (showProgressDialog) {
                        dialog.dismiss();
                    }
                    callBack.onSuccessApiCall(jsonObject, tag, statusCode);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                if (showProgressDialog) {
                    dialog.dismiss();
                }
                callBack.onErrorApiCall(new JSONObject(), tag, statusCode);
                if (error instanceof NetworkError) {
                    Dialogs.getInstance().showAlert(context, "", "Network error");
                } else if (error instanceof ServerError) {
//                     Utils.printLog("ServerError", "volley timeout+++");
                } else if (error instanceof AuthFailureError) {
//                     Utils.printLog("AuthFailureError", "volley timeout");
                } else if (error instanceof ParseError) {
//                     Utils.printLog("ParseError", "volley timeout");
                } else if (error instanceof NoConnectionError) {
//                     Utils.printLog("NoConnectionError", "volley timeout");
                } else if (error instanceof TimeoutError) {
//                     Utils.printLog("TimeoutError", "volley timeout");
                }

            }


        }) {

            @Override
            protected Response<JSONObject> parseNetworkResponse(NetworkResponse response) {
                statusCode = response.statusCode;
                return super.parseNetworkResponse(response);
            }
        };
        request.setRetryPolicy(new RetryPolicy() {
            @Override
            public int getCurrentTimeout() {
                return requestTimeout;
            }

            @Override
            public int getCurrentRetryCount() {
                return requestTimeout;
            }

            @Override
            public void retry(VolleyError volleyError) throws VolleyError {
                }
        });
        UDTalkApplication.getInstance().addToRequestQueue(request, tag);
    }

    /*
     * GET request
     * @param Context
     * @param URL to server
     * @param Callback
     * */
    public void getApiCall(final Context context, final String url, final VolleyWebserviceCallBack callBack, final String tag) {
        String tag_json_obj = context.getClass().getSimpleName(); // tag will be requied to identify request

        StringRequest gsonRequest = new StringRequest(url,
                new Response.Listener<String>() {

                    @Override
                    public void onResponse(String serverJson) {
                        try {
                            callBack.onSuccessApiCall(new JSONObject(serverJson), tag, statusCode);
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                error.printStackTrace();
                if (error instanceof NetworkError) {
                } else if (error instanceof ServerError) {
                } else if (error instanceof AuthFailureError) {
                } else if (error instanceof ParseError) {
                } else if (error instanceof NoConnectionError) {
                } else if (error instanceof TimeoutError) {
                }
            }
        });
        UDTalkApplication.getInstance().addToRequestQueue(gsonRequest, tag_json_obj);
    }

}