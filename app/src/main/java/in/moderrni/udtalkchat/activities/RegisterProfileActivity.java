package in.moderrni.udtalkchat.activities;

import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.os.Bundle;
import android.os.StrictMode;
import android.provider.MediaStore;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;


import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import de.hdodenhof.circleimageview.CircleImageView;
import in.moderrni.udtalkchat.R;
import in.moderrni.udtalkchat.interfaces.VolleyWebserviceCallBack;
import in.moderrni.udtalkchat.models.ProfileModelClass;
import in.moderrni.udtalkchat.models.RegisterMobileModel;
import in.moderrni.udtalkchat.ui.customeviews.SpinnerCustomHintAdapter;
import in.moderrni.udtalkchat.utils.Constants;
import in.moderrni.udtalkchat.utils.Utility;


public class RegisterProfileActivity extends AppCompatActivity implements VolleyWebserviceCallBack {
    @BindView(R.id.sp_career)
    Spinner sp_career;
    @BindView(R.id.et_name)
    EditText et_name;
    @BindView(R.id.et_companyName)
    EditText et_companyName;
    @BindView(R.id.et_products)
    EditText et_products;
    @BindView(R.id.btn_continue)
    View btn_continue;
    @BindView(R.id.profile_pic)
    CircleImageView profile_pic;
    String[] career = {"Select Your UDtalks ID", "Agent", "Buyer", "Seller", "Friend"};
    ArrayList<String> careerAl = new ArrayList<>();
    Bitmap bitmap = null;
    private String mobile;
    private RegisterMobileModel response;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register_profile);
        ButterKnife.bind(this);
        StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
        StrictMode.setThreadPolicy(policy);
        careerAl.clear();
        careerAl.add("Select Your UDtalks ID");
        careerAl.add("Agent");
        careerAl.add("Buyer");
        careerAl.add("Seller");
        careerAl.add("Friend");
        response = getIntent().getParcelableExtra(Constants.USER_DATA);
        SpinnerCustomHintAdapter adapter = new SpinnerCustomHintAdapter(this, android.R.layout.simple_spinner_item, career);
        adapter.setDropDownViewResource(R.layout.spinner_item);
        sp_career.setAdapter(adapter);

        sp_career.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                String selectedItemText = (String) parent.getItemAtPosition(position);
                // If user change the default selection
                // First item is disable and it is used for hint
                TextView tv = (TextView) view;
                tv.setPadding(0, 0, 0, 0);
                if (position > 0) {
                    tv.setTextColor(Color.BLACK);
                } else {
                    tv.setTextColor(Color.GRAY);
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });


//        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
//        sp_career.setAdapter(adapter);
    }


    @OnClick(R.id.btn_continue)
    void registerNewUser() {
        if (validateFields()) {
            registerNewUDUser();
        }
    }

    public boolean validateFields() {
        if (sp_career.getSelectedItemPosition() == 0) {
            Utility.showSnackBar(et_companyName, getResources().getString(R.string.please_select_udtalks_id));
            return false;
        } else if (Utility.isEmptyString(et_name.getText().toString())) {
            Utility.showSnackBar(et_companyName, getResources().getString(R.string.please_enter_your_name));
            return false;
        } else if (Utility.isEmptyString(et_companyName.getText().toString())) {
            Utility.showSnackBar(et_companyName, getResources().getString(R.string.please_enter_company_name));
            return false;
        } else if (Utility.isEmptyString(et_products.getText().toString())) {
            Utility.showSnackBar(et_companyName, getResources().getString(R.string.please_enter_products));
            return false;
        } else {
            return true;
        }
    }

    @Override
    public void onSuccessApiCall(JSONObject serverResult, String requestTag, int statusCode) {
        ProfileModelClass detail = new ProfileModelClass();
        try {
            if (serverResult.getBoolean(Constants.status)) {
                detail.setCareer(careerAl.get(sp_career.getSelectedItemPosition()));
                detail.setCompany(et_companyName.getText().toString());
                detail.setUserId(serverResult.getString("user_id"));
                detail.setProducts(et_products.getText().toString());
                detail.setProfilePic(serverResult.getString("profile_pic"));
                detail.setCountry(response.getCountryName());
                detail.setNickname(et_name.getText().toString());
//                registerUser(mobile, detail);
//                Utility.setSharedPreference(this, Constants.USER_ID, serverResult.getString(Constants.USER_ID));
//                Utility.setSharedPreference(this, Constants.NICKNAME, et_name.getText().toString());
//                Utility.setSharedPreference(this, Constants.MOBILE, mobile);
////
//
                navigateToHome(detail);
            }
        } catch (JSONException e) {
            e.printStackTrace();
        } catch (/*XmppStringprep*/Exception e) {
            e.printStackTrace();
        }
    }


    private void registerNewUDUser() {
        try {
            mobile = response.getCountryCode() + response.getMobileNumber();
            mobile = mobile.contains("+") ? mobile : "+" + mobile;
            JSONObject sendData = new JSONObject();
            sendData.put(Constants.NICKNAME, et_name.getText().toString());
            sendData.put("verification_code", response.getOtp());
            sendData.put("country", response.getCountryName());
            sendData.put("career", "" + careerAl.get(sp_career.getSelectedItemPosition()));
            sendData.put("products", et_products.getText().toString());
            sendData.put("mobile_no", mobile);
            sendData.put("password", "1");
            Bitmap icon = BitmapFactory.decodeResource(getResources(), R.drawable.user_placeholder);
            sendData.put("profile_pic", Utility.encodeTobase64(null != bitmap ? bitmap : icon));
            sendData.put("company", et_companyName.getText().toString());
            String url = Constants.BASE_URL + Constants.ACTIVATE_USER;
            Utility.printLog(Constants.ACTIVATE_USER, sendData.toString());
            VolleyWebserviceCall.getInstance().postApiCall(this, url, sendData, RegisterProfileActivity.this, true, Constants.ACTIVATE_USER);
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    private void navigateToHome(ProfileModelClass detail) {
        Intent i = new Intent(this, MainActivity.class);
        i.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        i.putExtra(Constants.USER_DATA, response);
        /* OLD TEAM CODEDBManager.getInstance(this).addOrUpdateRowInDB(detail);
        Utility.saveUserJsonObj(detail, this);*/
        i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);

        startActivity(i);
        finish();



    }

    @Override
    public void onErrorApiCall(JSONObject serverResult, String requestTag, int statusCode) {

    }

}
