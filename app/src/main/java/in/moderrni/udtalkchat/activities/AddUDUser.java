package in.moderrni.udtalkchat.activities;

import android.support.constraint.ConstraintLayout;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import in.moderrni.udtalkchat.R;
import in.moderrni.udtalkchat.fragments.ChatFragment;
import in.moderrni.udtalkchat.models.User;
import in.moderrni.udtalkchat.ui.adapters.AddUDUserDataAdapter;
import in.moderrni.udtalkchat.ui.adapters.AllUsersDataAdapter;
import in.moderrni.udtalkchat.utils.Utility;
import in.moderrni.udtalkchat.utils.core.AppSingletonDatabase;
import io.reactivex.Observable;
import io.reactivex.Observer;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;

public class AddUDUser extends BaseActivity {

    View view;
    @BindView(R.id.addudrecyclerview_id)
    RecyclerView recyclerView;
    @BindView(R.id.tvnodata_id)
    TextView textviewNodata;
    //User image views
    @BindView(R.id.ivagent_id)
    ImageView iv_agent;
    @BindView(R.id.ivseller_id)
    ImageView iv_seller;
    @BindView(R.id.ivbuyer_id)
    ImageView iv_buyer;
    @BindView(R.id.ivfriend_id)
    ImageView iv_friend;

    //user text views
    @BindView(R.id.tvseller_id)
    TextView tv_Seller;
    @BindView(R.id.tvbuyer_id)
    TextView tv_buyer;
    @BindView(R.id.tvagent_id)
    TextView tv_agent;
    @BindView(R.id.tvfriend_id)
    TextView tv_friend;

    //cancle button
    @BindView(R.id.iv_clear)
    ImageView clear;
    @BindView(R.id.rootView)
    ConstraintLayout rootView;
    //searchedittext

    @BindView(R.id.et_search)
    EditText search;

    String UserSelected = "all";
    private static String TAG = MainActivity.class.getSimpleName();
    private AppSingletonDatabase appSingletonDatabase;
    LinearLayoutManager linearLayoutManager;
    AddUDUserDataAdapter allUsersDataAdapter;

    List<User> userlist = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_uduser);
        ButterKnife.bind(this);
        Utility.setupUI(rootView, this);
        iv_seller.setSelected(true);
        UserSelected = "Seller";
        tv_Seller.setTextColor(this.getResources().getColor(R.color.active_agent));
        tv_buyer.setTextColor(this.getResources().getColor(R.color.white));
        tv_agent.setTextColor(this.getResources().getColor(R.color.white));
        tv_friend.setTextColor(this.getResources().getColor(R.color.white));
        initview("Seller");
        SearchResult();


    }


    @OnClick({R.id.iv_clear})
    void clearText() {
        search.setText("");
        recyclerView.setVisibility(view.GONE);
        textviewNodata.setVisibility(view.VISIBLE);//gone text view
    }


    @OnClick({R.id.ivfriend_id, R.id.ivbuyer_id, R.id.ivseller_id, R.id.ivagent_id})
    void ViewClick(View v) {
        switch (v.getId()) {
            case R.id.ivseller_id:
                UserSelected = "Seller";
                tv_Seller.setTextColor(this.getResources().getColor(R.color.active_agent));
                tv_buyer.setTextColor(this.getResources().getColor(R.color.white));
                tv_agent.setTextColor(this.getResources().getColor(R.color.white));
                tv_friend.setTextColor(this.getResources().getColor(R.color.white));
                initview("Seller");
                textviewNodata.setVisibility(view.VISIBLE);
                SearchResult();

                break;

            case R.id.ivbuyer_id:
                UserSelected = "Buyer";
                tv_Seller.setTextColor(this.getResources().getColor(R.color.white));
                tv_buyer.setTextColor(this.getResources().getColor(R.color.active_agent));
                tv_agent.setTextColor(this.getResources().getColor(R.color.white));
                tv_friend.setTextColor(this.getResources().getColor(R.color.white));
                initview("Buyer");
                textviewNodata.setVisibility(view.VISIBLE);
                if (!search.getText().toString().isEmpty()) {

                    SearchResult();
                }
                break;

            case R.id.ivagent_id:
                UserSelected = "Agent";
                tv_Seller.setTextColor(this.getResources().getColor(R.color.white));
                tv_buyer.setTextColor(this.getResources().getColor(R.color.white));
                tv_agent.setTextColor(this.getResources().getColor(R.color.active_agent));
                tv_friend.setTextColor(this.getResources().getColor(R.color.white));
                textviewNodata.setVisibility(view.VISIBLE);
                initview("Agent");
                if (!search.getText().toString().isEmpty()) {

                    SearchResult();
                }
                break;

            case R.id.ivfriend_id:
                UserSelected = "Friend";
                tv_Seller.setTextColor(this.getResources().getColor(R.color.white));
                tv_buyer.setTextColor(this.getResources().getColor(R.color.white));
                tv_agent.setTextColor(this.getResources().getColor(R.color.white));
                tv_friend.setTextColor(this.getResources().getColor(R.color.active_agent));
                textviewNodata.setVisibility(view.VISIBLE);
                initview("Friend");
                if (!search.getText().toString().isEmpty()) {

                    SearchResult();
                }
                break;


        }
    }

    private void initview(String usertype) {

        recyclerView.setVisibility(view.INVISIBLE);//just invisible the recycler view
        appSingletonDatabase = AppSingletonDatabase.getINSTANCE(this);//create instance of room db
        linearLayoutManager = new LinearLayoutManager(this);
        recyclerView.setLayoutManager(linearLayoutManager);//set layout to recycler view
        allUsersDataAdapter = new AddUDUserDataAdapter(this);//set context to adapter
        recyclerView.setAdapter(allUsersDataAdapter);//set adapter to recycler view

        userlist = appSingletonDatabase.userDao().getUserTypeList(true, usertype);
        allUsersDataAdapter.SetList(userlist);//attach list to recycler view

    }

    private void SearchResult() {


        //adding a TextChangedListener
        //to call a method whenever there is some change on the EditText

        search.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

                if (!search.getText().toString().equalsIgnoreCase("")) {
                    allUsersDataAdapter.getFilter().filter(s); //call filter
                    textviewNodata.setVisibility(view.GONE);//gone text view
                    recyclerView.setVisibility(view.VISIBLE);//visible recycler view
                } else {
                    textviewNodata.setVisibility(view.VISIBLE);//gone text view
                    recyclerView.setVisibility(view.GONE);//visible recycler view
                }


            }

            @Override
            public void afterTextChanged(Editable s) {

            }

        });
    }
}
