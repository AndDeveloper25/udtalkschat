package in.moderrni.udtalkchat.activities;

import android.content.Context;
import android.util.Log;

import java.util.List;

//import io.realm.Realm;
//import io.realm.RealmConfiguration;
//import io.realm.RealmObject;
//import io.realm.RealmResults;
//import io.realm.Sort;

public class DBManager {
    private static final String TAG = DBManager.class.getSimpleName();
    private static final int REALM_VERSION = 1;
  //  private static Realm realm;
    private static DBManager instance;

//    private DBManager(Context context) {
//        String key = "!d3@!R@ho01raK3#!d3@!R@ho01raK3#!d3@!R@ho01raK3#!d3@!R@ho01raK3#";
//        if (realm == null) {
//
//            Realm.init(context);
//            RealmConfiguration config = new RealmConfiguration.Builder()
//                    .name("idea.activation")
//                    .schemaVersion(REALM_VERSION)
//                    .build();
//
//            Realm.setDefaultConfiguration(config);
//            realm = Realm.getInstance(config);
//
//        }
//
//    }
//
//    public static Realm getRealm() {
//        return realm;
//    }
//
//    public static DBManager getInstance(Context context) {
//        if (instance == null)
//            instance = new DBManager(context);
//
//        return instance;
//    }
//
//    public void addOrUpdateRecordsInDB(List list) {
//        Log.v(TAG, "addOrUpdateRecordsInDB:" + list.size() + " Realm Class name:" + list.getClass().getComponentType());
//
//        realm.beginTransaction();
//        realm.copyToRealmOrUpdate(list);
//        realm.commitTransaction();
//    }
//
//    public void addOrUpdateRowInDB(RealmObject realmObject) {
//        realm.beginTransaction();
//        realm.copyToRealmOrUpdate(realmObject);
//        realm.commitTransaction();
//    }
//
///*
//    public void updateDeliveryReceipt(String stanzaId) {
//        realm.beginTransaction();
//        ChatModel result = realm.where(ChatModel.class).equalTo("stanzaId", stanzaId).findFirst();
//        result.setDelivered(true);
//        realm.copyToRealmOrUpdate(result);
//        realm.commitTransaction();
//    }
//*/
//
//
//    public RealmResults getDataByClass(Class aClass) {
//        realm.beginTransaction();
//        RealmResults realmResults = realm.where(aClass).findAll();
//        realm.commitTransaction();
//        return realmResults;
//    }
//
//    public RealmResults getChatList(Class aClass) {
//        realm.beginTransaction();
//        RealmResults realmResults = realm.where(aClass).equalTo("blockStatus", false).findAllSorted("timestamp", Sort.ASCENDING);
//        realm.commitTransaction();
//        return realmResults;
//    }
//
//
//    public RealmResults getBlockList(Class aClass) {
//        realm.beginTransaction();
//        RealmResults realmResults = realm.where(aClass).equalTo("blockStatus", true).findAllSorted("timestamp", Sort.ASCENDING);
//        realm.commitTransaction();
//        return realmResults;
//    }
//
//
//
//
//
//
//
//    public void deleteById(Class aClass, String key, String value) {
//        realm.beginTransaction();
//        RealmResults result = realm.where(aClass).equalTo(key, value).findAll();
//        if (result != null) {
//            result.deleteAllFromRealm();
//        }
//        realm.commitTransaction();
//    }
//
//    public void deleteById(Class aClass, String key, int value) {
//        realm.beginTransaction();
//        RealmResults result = realm.where(aClass).equalTo(key, value).findAll();
//        if (result != null) {
//            result.deleteAllFromRealm();
//        }
//        realm.commitTransaction();
//    }
//
//    public void deleteAllData(Class aClass) {
//        realm.beginTransaction();
//        RealmResults result = realm.where(aClass).findAll();
//        result.deleteAllFromRealm();
//        realm.commitTransaction();
//    }
//






}