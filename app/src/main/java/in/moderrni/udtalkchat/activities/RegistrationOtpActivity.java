package in.moderrni.udtalkchat.activities;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.EditText;
import android.widget.LinearLayout;


import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import in.moderrni.udtalkchat.R;
import in.moderrni.udtalkchat.models.ProfileModelClass;
import in.moderrni.udtalkchat.models.RegisterMobileModel;
import in.moderrni.udtalkchat.models.UserDetail;
import in.moderrni.udtalkchat.utils.Constants;
import in.moderrni.udtalkchat.utils.Utility;

public class RegistrationOtpActivity extends AppCompatActivity {
    @BindView(R.id.btn_continue)
    View btn_continue;

    @BindView(R.id.rootView)
    LinearLayout rootView;
    @BindView(R.id.tv_otp)
    EditText tv_otp;
    private RegisterMobileModel response;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_registration_otp);
        ButterKnife.bind(this);
        Utility.setupUI(rootView, this);
        response = getIntent().getParcelableExtra(Constants.USER_DATA);
        tv_otp.setText(response.getOtp());
    }

    @OnClick(R.id.btn_continue)
    void openProfileActivity() {
        if (Utility.isEmptyString(tv_otp.getText().toString())) {
            Utility.showSnackBar(tv_otp, getResources().getString(R.string.please_enter_otp));
        } else if (!response.getOtp().equalsIgnoreCase(tv_otp.getText().toString())) {
            Utility.showSnackBar(tv_otp, getResources().getString(R.string.invalid_otp));
        } else {
            if (null != response && null != response.getUserDetails() && null != response.getUserDetails().get(0)) {
                Utility.setSharedPreference(RegistrationOtpActivity.this, Constants.USER_ID, response.getUserDetails().get(0).getUserId());
                Utility.setSharedPreference(this, Constants.NICKNAME, response.getUserDetails().get(0).getName());
                Utility.setSharedPreference(this, Constants.MOBILE, response.getCountryCode() + response.getMobileNumber());
            }
            if (Boolean.parseBoolean(response.getIsRegister())) {

                /*
                OLD TEAM REALM CODE -
                DBManager.getInstance(this).deleteAllData(ProfileModelClass.class);
                UserDetail udetail = response.getUserDetails().get(0);
                ProfileModelClass detail = new ProfileModelClass();
                detail.setProfilePic(udetail.getProfilePic());
                detail.setNickname(udetail.getName());
                detail.setCountry(udetail.getCountry());
                detail.setProducts(udetail.getProducts());
                detail.setCareer(udetail.getCareer());
                detail.setCompany(udetail.getCompany());
                detail.setUserId(udetail.getUserId());
                detail.setUserStatus(udetail.getUserStatus());
                detail.setPrivacyStatus(""+true);
                detail.setMobileNo(response.getCountryCode()+response.getMobileNumber());
                DBManager.getInstance(this).addOrUpdateRowInDB(detail);
                Utility.saveUserJsonObj(detail,this);
                */
            }
            Intent intent = new Intent(RegistrationOtpActivity.this, Boolean.parseBoolean(response.getIsRegister()) ? MainActivity.class : RegisterProfileActivity.class);
            //Intent intent=new Intent(RegistrationOtpActivity.this,MainActivity.class);
            intent.putExtra(Constants.USER_DATA, response);
            startActivity(intent);
        }
    }

}
