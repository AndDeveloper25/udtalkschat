package in.moderrni.udtalkchat.activities;


import android.os.Bundle;
import android.view.View;
import android.webkit.WebSettings;
import android.webkit.WebView;




import butterknife.BindView;
import in.moderrni.udtalkchat.R;

public class TermsConditions extends BaseActivity {
    @BindView(R.id.wvForTC)
    WebView wvForTC;
    boolean isTerms = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_terms_conditions);
        initViews();
    }

    private void initViews() {
        isTerms = getIntent().hasExtra("terms");
      //  iv_back.setVisibility(View.VISIBLE);
      //  tv_toolbar_title.setText(isTerms ? "Terms & Conditions" : "Privacy Policy");
        wvForTC = (WebView) findViewById(R.id.wvForTC);
        WebSettings settings = wvForTC.getSettings();
        settings.setDefaultTextEncodingName("utf-8");
        wvForTC.loadUrl("file:///android_asset/tc.html");
    }
}
