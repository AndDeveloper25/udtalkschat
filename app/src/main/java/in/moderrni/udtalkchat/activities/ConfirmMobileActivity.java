package in.moderrni.udtalkchat.activities;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.TextView;



import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import in.moderrni.udtalkchat.R;
import in.moderrni.udtalkchat.models.RegisterMobileModel;
import in.moderrni.udtalkchat.utils.Constants;
import in.moderrni.udtalkchat.utils.SharedPref;
import in.moderrni.udtalkchat.utils.Utility;

public class ConfirmMobileActivity extends AppCompatActivity {
    @BindView(R.id.tv_phoneNumber)
    TextView tv_phoneNumber;
    @BindView(R.id.btn_confirm)
    View btn_confirm;
    @BindView(R.id.btn_cancel)
    View btn_cancel;
    private RegisterMobileModel response;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_confirm_mobile);
        ButterKnife.bind(this);
        response= getIntent().getParcelableExtra(Constants.USER_DATA);
        tv_phoneNumber.setText("(" + response.getCountryCode() + ")" + response.getMobileNumber());
    }

    @OnClick(R.id.btn_confirm)
    void confirmNumber() {
        String mobilenumber=tv_phoneNumber.getText().toString();
        SharedPref.setSession(ConfirmMobileActivity.this,mobilenumber);
        Intent intent = new Intent(this, RegistrationOtpActivity.class);
        intent.putExtra(Constants.USER_DATA,response);
        startActivity(intent);
    }

    @OnClick(R.id.btn_cancel)
    void cancelNumber() {
        onBackPressed();
    }
}
