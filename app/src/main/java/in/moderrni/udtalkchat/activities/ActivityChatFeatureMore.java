package in.moderrni.udtalkchat.activities;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

import in.moderrni.udtalkchat.R;
import in.moderrni.udtalkchat.interfaces.ViewHolderClickListener;
import in.moderrni.udtalkchat.models.ChatMoreFeataresModel;
import in.moderrni.udtalkchat.ui.adapters.ChatFeaturesMoreAdapter;
import in.moderrni.udtalkchat.ui.adapters.CustomDropdownAdapter;

public class ActivityChatFeatureMore extends BaseActivity  {
    private List<ChatMoreFeataresModel> chatMoreFeataresModels = new ArrayList<>();
    private RecyclerView rvData;
    private ChatFeaturesMoreAdapter chatFeaturesMoreAdapter;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_chat_features_menu);
        rvData = (RecyclerView) findViewById(R.id.rvData);

        chatFeaturesMoreAdapter = new ChatFeaturesMoreAdapter(chatMoreFeataresModels,this);
        RecyclerView.LayoutManager gridLayoutManager = new GridLayoutManager(this, 3);
        rvData.setLayoutManager(gridLayoutManager);
        rvData.setItemAnimator(new DefaultItemAnimator());
        rvData.setAdapter(chatFeaturesMoreAdapter);

        prepareData();

    }

    private void prepareData() {
        ChatMoreFeataresModel chatMoreFeataresModel = new ChatMoreFeataresModel("sell post",R.drawable.ic_sell_post);
        chatMoreFeataresModels.add(chatMoreFeataresModel);

        chatMoreFeataresModel = new ChatMoreFeataresModel("Buy Post",R.drawable.iv_adduser);
        chatMoreFeataresModels.add(chatMoreFeataresModel);

         chatMoreFeataresModel = new ChatMoreFeataresModel("Movements",R.drawable.ic_movements);
        chatMoreFeataresModels.add(chatMoreFeataresModel);

        chatMoreFeataresModel = new ChatMoreFeataresModel("Images",R.drawable.ic_chat_images);
        chatMoreFeataresModels.add(chatMoreFeataresModel);

        chatMoreFeataresModel = new ChatMoreFeataresModel("Take Photo",R.drawable.ic_take_photo);
        chatMoreFeataresModels.add(chatMoreFeataresModel);

        chatMoreFeataresModel = new ChatMoreFeataresModel("Video",R.drawable.ic_video);
        chatMoreFeataresModels.add(chatMoreFeataresModel);

         chatMoreFeataresModel = new ChatMoreFeataresModel("Take Video",R.drawable.ic_take_video);
        chatMoreFeataresModels.add(chatMoreFeataresModel);

        chatMoreFeataresModel = new ChatMoreFeataresModel("Media Tools",R.drawable.ic_media_tools);
        chatMoreFeataresModels.add(chatMoreFeataresModel);

        chatMoreFeataresModel = new ChatMoreFeataresModel("Send Location",R.drawable.ic_send_location);
        chatMoreFeataresModels.add(chatMoreFeataresModel);

        chatMoreFeataresModel = new ChatMoreFeataresModel("Contact Info",R.drawable.ic_contact_info);
        chatMoreFeataresModels.add(chatMoreFeataresModel);

        chatMoreFeataresModel = new ChatMoreFeataresModel("Audio",R.drawable.ic_audio);
        chatMoreFeataresModels.add(chatMoreFeataresModel);

        chatMoreFeataresModel = new ChatMoreFeataresModel("File",R.drawable.ic_file);
        chatMoreFeataresModels.add(chatMoreFeataresModel);


    }


}
