package in.moderrni.udtalkchat.activities;

import android.app.Activity;
import android.app.AlertDialog;

import android.content.ActivityNotFoundException;
import android.content.ContentValues;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.provider.MediaStore;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import in.moderrni.udtalkchat.R;
import in.moderrni.udtalkchat.receiver.ConnectivityReceiver;
import in.moderrni.udtalkchat.utils.UDTalkApplication;


public class BaseActivity extends AppCompatActivity implements ConnectivityReceiver.ConnectivityReceiverListener {
    ConnectivityReceiver connectivityReceiver = new ConnectivityReceiver();
    /*Permissions code*/
    public static final int REQUEST_GALARY = 0;
    public static final int REQUEST_CAMARA = 1;
    public static final String ACCESS_STORAGE = android.Manifest.permission.WRITE_EXTERNAL_STORAGE;
    public static final String CAMERA = android.Manifest.permission.CAMERA;
    public static final String CALL_PHONE = android.Manifest.permission.CALL_PHONE;
    public static final String READ_CONTACTS = android.Manifest.permission.READ_CONTACTS;
    public static final String RECEIVE_SMS = android.Manifest.permission.RECEIVE_SMS;
    public static final String ACCESS_COARSE_LOCATION = android.Manifest.permission.ACCESS_COARSE_LOCATION;
    public static final String ACCESS_FINE_LOCATION = android.Manifest.permission.ACCESS_FINE_LOCATION;
    public static final int PERMISSION = 3;
    public static final int PIC_CROP = 2;
    Uri imageUri;

    @Override
    public void onNetworkConnectionChanged(boolean isConnected) {
        showNotConnectedStatus(isConnected);
    }

    public void showNotConnectedStatus(boolean isConnected) {
        AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(BaseActivity.this);
        AppCompatActivity appCompatActivity = (AppCompatActivity) BaseActivity.this;
        LayoutInflater inflater = appCompatActivity.getLayoutInflater();
        View dialogView = inflater.inflate(R.layout.standard_alert_dialog, null);
        dialogBuilder.setView(dialogView);
        TextView msg = dialogView.findViewById(R.id.txtMsg);
        Button btn = dialogView.findViewById(R.id.btn);

        btn.setText(BaseActivity.this.getString(R.string.retry));
        msg.setText(BaseActivity.this.getString(R.string.disconnected_internet));
        final AlertDialog alertDialog = dialogBuilder.create();
        alertDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        alertDialog.setCanceledOnTouchOutside(false);
        btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (isNetworkAvailable())
                    alertDialog.dismiss();
                else {
                    alertDialog.dismiss();
                    alertDialog.show();
                }

            }
        });
        if (!isConnected) {
            alertDialog.show();
        }

    }

    public boolean isNetworkAvailable() {
        ConnectivityManager connectivityManager
                = (ConnectivityManager) BaseActivity.this.getSystemService(BaseActivity.this.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
        return activeNetworkInfo != null && activeNetworkInfo.isConnected();
    }

    @Override
    protected void onStart() {
        super.onStart();
        //register connection
        final IntentFilter intentFilter = new IntentFilter();
        intentFilter.addAction(ConnectivityManager.CONNECTIVITY_ACTION);
        registerReceiver(connectivityReceiver, intentFilter);
        // register connection status listener
        UDTalkApplication.getInstance().setConnectivityListener(this);
    }

    @Override
    protected void onStop() {
        super.onStop();
        //unregister the connection
        unregisterReceiver(connectivityReceiver);
    }

    public boolean checkPermission() {
        int accessStorege = ContextCompat.checkSelfPermission(getApplicationContext(), ACCESS_STORAGE);
        int camara = ContextCompat.checkSelfPermission(getApplicationContext(), CAMERA);
        int callPhone = ContextCompat.checkSelfPermission(getApplicationContext(), CALL_PHONE);
        int readContacts = ContextCompat.checkSelfPermission(getApplicationContext(), READ_CONTACTS);
        // int receiveSMS = ContextCompat.checkSelfPermission(getApplicationContext(), RECEIVE_SMS);
        //int accessCoarseLoc = ContextCompat.checkSelfPermission(getApplicationContext(), ACCESS_COARSE_LOCATION);
        // int accessFineLoc = ContextCompat.checkSelfPermission(getApplicationContext(), ACCESS_FINE_LOCATION);

       /* return accessStorege == PackageManager.PERMISSION_GRANTED && camara == PackageManager.PERMISSION_GRANTED
                && callPhone == PackageManager.PERMISSION_GRANTED && readContacts == PackageManager.PERMISSION_GRANTED
                && receiveSMS == PackageManager.PERMISSION_GRANTED && accessCoarseLoc == PackageManager.PERMISSION_GRANTED
                && accessFineLoc == PackageManager.PERMISSION_GRANTED;*/
        return accessStorege == PackageManager.PERMISSION_GRANTED && camara == PackageManager.PERMISSION_GRANTED
                && callPhone == PackageManager.PERMISSION_GRANTED && readContacts == PackageManager.PERMISSION_GRANTED
                ;
    }

    public void requestPermission() {
        //    ActivityCompat.requestPermissions(this, new String[]{ACCESS_STORAGE, CAMERA, CALL_PHONE, READ_CONTACTS, RECEIVE_SMS, ACCESS_COARSE_LOCATION, ACCESS_FINE_LOCATION}, PERMISSION);
        ActivityCompat.requestPermissions(this, new String[]{ACCESS_STORAGE, CAMERA, CALL_PHONE, READ_CONTACTS},PERMISSION);
    }




}
