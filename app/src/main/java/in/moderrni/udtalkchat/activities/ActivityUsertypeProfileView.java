package in.moderrni.udtalkchat.activities;

import android.content.Intent;
import android.os.Bundle;
import android.support.constraint.ConstraintLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.RecyclerView;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import in.moderrni.udtalkchat.R;
import in.moderrni.udtalkchat.models.User;
import in.moderrni.udtalkchat.ui.adapters.AllUsersDataAdapter;
import in.moderrni.udtalkchat.ui.customeviews.CustomTextView;
import in.moderrni.udtalkchat.utils.Constants;
import in.moderrni.udtalkchat.utils.core.AppSingletonDatabase;

public class ActivityUsertypeProfileView extends AppCompatActivity {

    @BindView(R.id.tvCompanyName)
    TextView companyname;
    @BindView(R.id.ivUserPhoto)
    ImageView userprofile;
    @BindView(R.id.tvUserName)
    TextView username;
    @BindView(R.id.tvUserGender)
    TextView usergender;
    @BindView(R.id.tvUserAdress)
    TextView useraddress;
    @BindView(R.id.tvCountryName)
    TextView countryname;
    @BindView(R.id.tvDateOfBirth)
    TextView dateofbirth;
    @BindView(R.id.tvMyProducts)
    TextView myproducts;
    @BindView(R.id.rvActivityPosts)
    RecyclerView postrecyclerview;
    @BindView(R.id.ivContact)
    ImageView contact;
    @BindView(R.id.ivSellerChat)
    ImageView sellerchat;
    @BindView(R.id.ivAddSeller)
    ImageView addseller;
    @BindView(R.id.tvToolBarSeller)
    CustomTextView titleontoolbar;
    List<User> userlist = new ArrayList<>();
    private AppSingletonDatabase appSingletonDatabase;
    String mobilenumber;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_seller_screen);
        ButterKnife.bind(this);
        appSingletonDatabase = AppSingletonDatabase.getINSTANCE(this);
        if (getIntent().getExtras() != null) {
            int id = getIntent().getExtras().getInt(Constants.PROFILE_ID);
            setDetails(id);
        }


    }

    private void setDetails(int id) {
        User user = appSingletonDatabase.userDao().getuserDetail(1, id);
        companyname.setText(user.getCompany());
        username.setText(user.getNickname());
        // usergender.setText(user.get);
        //useraddress.setText(user.);
        countryname.setText(user.getCountry());
        //dateofbirth.setText(user.);
        myproducts.setText(user.getProducts());
        mobilenumber = user.getMobile_no();
        titleontoolbar.setText(user.getTypeOfUser().toUpperCase());
    }


  /*  @Override
    public void sendId(User user) {
        companyname.setText(user.getCompany());
        username.setText(user.getNickname());
        // usergender.setText(user.get);
        //useraddress.setText(user.);
        countryname.setText(user.getCountry());
        //dateofbirth.setText(user.);
        myproducts.setText(user.getProducts());
        mobilenumber = user.getMobile_no();

    }*/
}
