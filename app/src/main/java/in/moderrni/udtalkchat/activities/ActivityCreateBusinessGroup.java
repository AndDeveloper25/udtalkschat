package in.moderrni.udtalkchat.activities;


import android.content.ActivityNotFoundException;
import android.content.ContentValues;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.annotation.Nullable;
import android.support.constraint.ConstraintLayout;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.Toast;

import com.bumptech.glide.Glide;

import java.io.File;

import butterknife.BindView;
import butterknife.ButterKnife;
import de.hdodenhof.circleimageview.CircleImageView;
import in.moderrni.udtalkchat.R;
import in.moderrni.udtalkchat.ui.customeviews.CustomButtonView;
import in.moderrni.udtalkchat.ui.customeviews.CustomEditText;
import in.moderrni.udtalkchat.utils.Utility;


public class ActivityCreateBusinessGroup extends BaseActivity implements View.OnClickListener {
    final int PIC_CROP = 3;
    private Uri picUri;
    @BindView(R.id.btnNext)
    CustomButtonView btnNext;
    @BindView(R.id.ivUserDp)
    CircleImageView imageView;
    @BindView(R.id.etGroupName)
    CustomEditText etGroupName;
    @BindView(R.id.etGroupDescription)
    CustomEditText etGroupDescription;
    @BindView(R.id.ivAdduser)
    ImageView ivAdduser;
    @BindView(R.id.ivSearch)
    ImageView ivSearch;
    @BindView(R.id.rootView)
    ConstraintLayout rootView;
    String strGrpName, strGrpDesc;


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.create_business_group);
        ButterKnife.bind(this);
        setListener();
    }


    private void setListener() {
        Utility.setupUI(rootView, this);
        imageView.setTag("0");//for image validation
        ivAdduser.setVisibility(View.GONE);
        ivSearch.setVisibility(View.GONE);
        imageView.setOnClickListener(this);
        btnNext.setOnClickListener(this);
    }

    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.ivUserDp:
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {

                    if (checkPermission()) {
                        // Toast.makeText(this, "check", Toast.LENGTH_SHORT).show();
                        selectImageDailog("Group Icon", this);

                    } else {
                        requestPermission();
                    }
                } else {
                    selectImageDailog("Group Icon", this);

                }
                break;
            case R.id.btnNext:
                if (checkImage() && validateForm()) {
                    Intent intent = new Intent(this, ActivityGroupMembersSelection.class);
                    startActivity(intent);
                }
                break;

        }
    }

    private boolean checkImage() {
        boolean isValid = true;
        if (imageView.getTag().equals("0")) {
            Utility.showSnackBar(imageView, getResources().getString(R.string.please_set_group_icon));
            isValid = false;
        } else
            imageView.setBackgroundResource(0);
        return isValid;
    }

    private boolean validateForm() {
        boolean isFormValid = true;
        initVarsFromForm();
        if (TextUtils.isEmpty(strGrpDesc)) {
            Utility.showSnackBar(etGroupDescription, getResources().getString(R.string.empty_grp_desc));
            isFormValid = false;
        }
        if (TextUtils.isEmpty(strGrpName)) {
            Utility.showSnackBar(etGroupName, getResources().getString(R.string.empty_grp_name));
            isFormValid = false;
        }
        return isFormValid;
    }

    private void initVarsFromForm() {
        strGrpName = etGroupName.getText().toString().trim();
        strGrpDesc = etGroupDescription.getText().toString().trim();
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        //Toast.makeText(getActivity(), "" + requestCode, Toast.LENGTH_SHORT).show();
        switch (requestCode) {
            case REQUEST_CAMARA:
                if (resultCode == RESULT_OK) {
                    //imgBank.setImageURI(imageUri);
                    Glide.with(this).load(imageUri).into(imageView);
                    imageView.setTag("1");
                    performCrop(imageUri);
                }

                break;
            case REQUEST_GALARY:
                if (resultCode == RESULT_OK) {
                    imageUri = data.getData();
                    //imgBank.setImageURI(imageUri);
                    if (imageUri != null) {
                        Glide.with(this).load(imageUri).into(imageView);
                        imageView.setTag("1");
                        performCrop(imageUri);
                    }
                }
                break;
            case PIC_CROP:
                if (resultCode == RESULT_OK) {
                    Toast.makeText(this, "text", Toast.LENGTH_SHORT).show();
                    if (data != null) {
                        // get the returned data
                        Bundle extras = data.getExtras();
                        // get the cropped bitmap
                        if (extras != null) {
                            Bitmap selectedBitmap = (Bitmap) extras.get("data");
                            imageView.setImageBitmap(selectedBitmap);
                            imageView.setTag("1");
                        }
                    }

                }
                break;
        }
    }

    @Override
    protected void onStart() {
        super.onStart();

    }

    public void selectImageDailog(String caption, Context context) {
        //Log.d(TAG, "selectImageDailog: ");
        final CharSequence[] options = {"Take Photo", "Choose from Gallery", "Cancel"};
        android.support.v7.app.AlertDialog.Builder builder = new android.support.v7.app.AlertDialog.Builder(context);
        builder.setCancelable(true);
        builder.setTitle("Add Photo (" + caption + ")");
        builder.setItems(options, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int item) {
                //camara
                if (options[item].equals("Take Photo")) {
                    Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                    ContentValues values = new ContentValues(1);
                    values.put(MediaStore.Images.Media.MIME_TYPE, "image/jpg");
                    //if (imageUri == null)
                    imageUri = getContentResolver().insert(MediaStore.Images.Media.EXTERNAL_CONTENT_URI, values);
                    intent.putExtra(MediaStore.EXTRA_OUTPUT, imageUri); // set the
                    startActivityForResult(intent, REQUEST_CAMARA);
                }
                //galary
                else if (options[item].equals("Choose from Gallery")) {
                    Intent pickPhoto = new Intent(Intent.ACTION_PICK, android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                    startActivityForResult(pickPhoto, REQUEST_GALARY);

                } else if (options[item].equals("Cancel")) {
                    dialog.dismiss();
                }
            }

        });
        builder.show();

    }

    public void performCrop(Uri picUri) {
        try {
            Intent cropIntent = new Intent("com.android.camera.action.CROP");
            // indicate image type and Uri
            cropIntent.setDataAndType(picUri, "image/*");
            // set crop properties here
            cropIntent.putExtra("crop", true);
            // indicate aspect of desired crop
            cropIntent.putExtra("aspectX", 1);
            cropIntent.putExtra("aspectY", 1);
            // indicate output X and Y
            cropIntent.putExtra("outputX", 256);
            cropIntent.putExtra("outputY", 256);
            // retrieve data on return
            cropIntent.putExtra("return-data", true);
            // start the activity - we handle returning in onActivityResult
            startActivityForResult(cropIntent, PIC_CROP);
        }
        // respond to users whose devices do not support the crop action
        catch (ActivityNotFoundException anfe) {
            // display an error message
            String errorMessage = "Whoops - your device doesn't support the crop action!";
            Toast toast = Toast.makeText(this, errorMessage, Toast.LENGTH_SHORT);
            toast.show();
        }
    }

    public String getPathFromURI(Uri contentUri) {
        Cursor cursor = getContentResolver().query(contentUri, null, null, null, null);
        assert cursor != null;
        cursor.moveToFirst();
        String document_id = cursor.getString(0);
        document_id = document_id.substring(document_id.lastIndexOf(":") + 1);
        cursor.close();
        cursor = getContentResolver().query(
                MediaStore.Images.Media.EXTERNAL_CONTENT_URI,
                null, MediaStore.Images.Media._ID + " = ? ", new String[]{document_id}, null);
        cursor.moveToFirst();
        String path = cursor.getString(cursor.getColumnIndex(MediaStore.Images.Media.DATA));
        cursor.close();
        return path;
    }

}








