package in.moderrni.udtalkchat.activities;

import android.app.Activity;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.WindowManager;

import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.PopupWindow;
import android.widget.Toast;

import java.util.ArrayList;

import in.moderrni.udtalkchat.models.ChatMoreFeataresModel;

import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import in.moderrni.udtalkchat.R;

import in.moderrni.udtalkchat.interfaces.ViewHolderClickListener;
import in.moderrni.udtalkchat.models.ChatMoreFeataresModel;


import in.moderrni.udtalkchat.models.DropdownModel;
import in.moderrni.udtalkchat.ui.adapters.ChatFeaturesMoreAdapter;
import in.moderrni.udtalkchat.ui.adapters.CustomDropdownAdapter;


public class ActivityChat extends BaseActivity implements View.OnClickListener, ViewHolderClickListener {


    @BindView(R.id.ivSetting)
    ImageView ivSetting;

    @BindView(R.id.userProfileBar)
    android.support.constraint.ConstraintLayout toolbar;

    ChatFeaturesMoreAdapter chatFeaturesMoreAdapter;
    private PopupWindow popupDialog;
    int SELECT_IMAGE = 1;
    private String selectedImagePath;
    private PopupWindow popupDialogadddialoge;
    @BindView(R.id.ivOptionMenu)
    ImageView optionMenu;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_chat);
        ButterKnife.bind(this);
        ivSetting.setOnClickListener(this);
        optionMenu.setOnClickListener(this);


    }


    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case (R.id.ivOptionMenu):
                OpenOptionMenu();
                break;

            case (R.id.ivSetting):
                OpenUserAddDialoge();
                break;


        }
    }

    private void OpenOptionMenu() {
        LayoutInflater inflater = LayoutInflater.from(this);
        View layout = inflater.inflate(R.layout.activity_chat_features_menu, null);
        popupDialog = new PopupWindow();
        popupDialog.setContentView(layout);
        popupDialog.setWidth(WindowManager.LayoutParams.MATCH_PARENT);
        popupDialog.setHeight(WindowManager.LayoutParams.MATCH_PARENT);
        popupDialog.setFocusable(true);
        popupDialog.setBackgroundDrawable(null);//for removing borders and margins
        popupDialog.showAsDropDown(toolbar);
        RecyclerView rvData = (RecyclerView) layout.findViewById(R.id.rvData);
        List<ChatMoreFeataresModel> chatMoreFeataresModels = new ArrayList<>();
        ChatFeaturesMoreAdapter chatFeaturesMoreAdapter = new ChatFeaturesMoreAdapter(chatMoreFeataresModels, this);
        chatFeaturesMoreAdapter.setOnItemclickListner(this);
        RecyclerView.LayoutManager gridLayoutManager = new GridLayoutManager(this, 3);
        rvData.setLayoutManager(gridLayoutManager);
        rvData.setItemAnimator(new DefaultItemAnimator());

        ChatMoreFeataresModel chatMoreFeataresModel = new ChatMoreFeataresModel("Sell Post", R.drawable.ic_sell_post);
        chatMoreFeataresModels.add(chatMoreFeataresModel);

        chatMoreFeataresModel = new ChatMoreFeataresModel("Buy Post", R.drawable.iv_adduser);
        chatMoreFeataresModels.add(chatMoreFeataresModel);

        chatMoreFeataresModel = new ChatMoreFeataresModel("Movements", R.drawable.ic_movements);
        chatMoreFeataresModels.add(chatMoreFeataresModel);

        chatMoreFeataresModel = new ChatMoreFeataresModel("Images", R.drawable.ic_chat_images);
        chatMoreFeataresModels.add(chatMoreFeataresModel);

        chatMoreFeataresModel = new ChatMoreFeataresModel("Take Photo", R.drawable.ic_take_photo);
        chatMoreFeataresModels.add(chatMoreFeataresModel);

        chatMoreFeataresModel = new ChatMoreFeataresModel("Video", R.drawable.ic_video);
        chatMoreFeataresModels.add(chatMoreFeataresModel);

        chatMoreFeataresModel = new ChatMoreFeataresModel("Take Video", R.drawable.ic_take_video);
        chatMoreFeataresModels.add(chatMoreFeataresModel);

        chatMoreFeataresModel = new ChatMoreFeataresModel("Media Tools", R.drawable.ic_media_tools);
        chatMoreFeataresModels.add(chatMoreFeataresModel);

        chatMoreFeataresModel = new ChatMoreFeataresModel("Send Location", R.drawable.ic_send_location);
        chatMoreFeataresModels.add(chatMoreFeataresModel);

        chatMoreFeataresModel = new ChatMoreFeataresModel("Contact Info", R.drawable.ic_contact_info);
        chatMoreFeataresModels.add(chatMoreFeataresModel);

        chatMoreFeataresModel = new ChatMoreFeataresModel("Audio", R.drawable.ic_audio);
        chatMoreFeataresModels.add(chatMoreFeataresModel);

        chatMoreFeataresModel = new ChatMoreFeataresModel("File", R.drawable.ic_file);
        chatMoreFeataresModels.add(chatMoreFeataresModel);
        rvData.setAdapter(chatFeaturesMoreAdapter);
        chatFeaturesMoreAdapter.notifyDataSetChanged();
        View popup_background = layout.findViewById(R.id.popup_background);
        popup_background.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                popupDialog.dismiss();
            }
        });
        popupDialog.setOnDismissListener(new PopupWindow.OnDismissListener() {
            @Override
            public void onDismiss() {
                // iv_add_user.setImageResource(R.drawable.ic_add_dots);
            }
        });

    }


    private void OpenUserAddDialoge() {
        LayoutInflater inflater = LayoutInflater.from(this);
        View layout = inflater.inflate(R.layout.layout_dropdown_adduser, null);
        popupDialogadddialoge = new PopupWindow();
        popupDialogadddialoge.setContentView(layout);
        popupDialogadddialoge.setWidth(WindowManager.LayoutParams.MATCH_PARENT);
        popupDialogadddialoge.setHeight(WindowManager.LayoutParams.MATCH_PARENT);
        popupDialogadddialoge.setFocusable(true);
        popupDialogadddialoge.setBackgroundDrawable(null);//for removing borders and margins
        popupDialogadddialoge.showAsDropDown(toolbar);
        ListView listPopup1 = (ListView) layout.findViewById(R.id.listPopup);
        ArrayList<DropdownModel> al = new ArrayList<>();
        al.add(new DropdownModel(R.drawable.ic_add_to_phone_contacts, getResources().getString(R.string.add_to_phone_contacts)));
        al.add(new DropdownModel(R.drawable.ic_search, getResources().getString(R.string.search)));
        al.add(new DropdownModel(R.drawable.ic_media, getResources().getString(R.string.media)));
        al.add(new DropdownModel(R.drawable.ic_mute_notification, getResources().getString(R.string.mute_notification)));
        al.add(new DropdownModel(R.drawable.ic_add_wallpaper, getResources().getString(R.string.add_wallpaper)));
        al.add(new DropdownModel(R.drawable.ic_clear_chat, getResources().getString(R.string.clear_chat)));
        al.add(new DropdownModel(R.drawable.ic_add_user, getResources().getString(R.string.translation_setting)));

        View popup_background = layout.findViewById(R.id.popup_background);
        popup_background.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                popupDialogadddialoge.dismiss();
            }
        });
        CustomDropdownAdapter adapter = new CustomDropdownAdapter(this, al, false);
        listPopup1.setAdapter(adapter);
        listPopup1.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                switch (position) {
                    case 0:

                        Toast.makeText(getApplicationContext(), "add_to_phone_contacts", Toast.LENGTH_SHORT).show();
                        break;
                    case 1:


                        Toast.makeText(getApplicationContext(), "search", Toast.LENGTH_SHORT).show();

                        break;
                    case 2:
                        Toast.makeText(getApplicationContext(), "media", Toast.LENGTH_SHORT).show();

                        break;
                    case 3:

                        Toast.makeText(getApplicationContext(), "mute_notification", Toast.LENGTH_SHORT).show();
                        break;

                    case 4:

                        Toast.makeText(getApplicationContext(), "add_wallpaper", Toast.LENGTH_SHORT).show();

                        break;
                    case 5:
                        Toast.makeText(getApplicationContext(), "clear_chat", Toast.LENGTH_SHORT).show();

                        break;
                    case 6:

                        Toast.makeText(getApplicationContext(), "translation_setting", Toast.LENGTH_SHORT).show();
                        break;


                }
                popupDialogadddialoge.dismiss();
            }

        });
        popupDialogadddialoge.setOnDismissListener(new PopupWindow.OnDismissListener() {
            @Override
            public void onDismiss() {
                // iv_add_user.setImageResource(R.drawable.ic_add_dots);
            }
        });



    }

    @Override
    public void setItemViewClick(int position) {//override interface method
        final Intent intent;
        switch (position) {
            case 0:
                startActivity(new Intent(this, ActivitySellPost.class));
                Toast.makeText(this, "sell post call", Toast.LENGTH_LONG).show();
                break;
            case 1:
                Toast.makeText(this, "Buy  post call", Toast.LENGTH_LONG).show();
                break;
            case 2:
                Toast.makeText(this, "Movements post call", Toast.LENGTH_LONG).show();
                break;
            case 3:
                //call image from gallary
                Intent imageIntent = new Intent();
                imageIntent.setType("image/*");
                imageIntent.setAction(Intent.ACTION_GET_CONTENT);//
                startActivityForResult(Intent.createChooser(imageIntent, "Select Picture"), SELECT_IMAGE);
                Toast.makeText(this, "Image call", Toast.LENGTH_LONG).show();
                break;
            case 4:
                Intent cameraintent = new Intent("android.media.action.IMAGE_CAPTURE");
                startActivity(cameraintent);
                Toast.makeText(this, "take photo call", Toast.LENGTH_LONG).show();
                break;

            case 5:
                Toast.makeText(this, "Video call", Toast.LENGTH_LONG).show();
                break;
            case 6:
                Toast.makeText(this, "Take video call", Toast.LENGTH_LONG).show();
                break;
            case 7:
                Toast.makeText(this, "Media tool call", Toast.LENGTH_LONG).show();
                break;

            case 8:
                Toast.makeText(this, "send location call", Toast.LENGTH_LONG).show();
                break;
            case 9:
                Toast.makeText(this, "Contact info call", Toast.LENGTH_LONG).show();
                break;
            case 10:
                Toast.makeText(this, "Audio call", Toast.LENGTH_LONG).show();
                break;
            case 11:
                Toast.makeText(this, "file call", Toast.LENGTH_LONG).show();
                break;
        }
    }

    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (resultCode == RESULT_OK) {
            if (requestCode == SELECT_IMAGE) {
                Uri selectedImageUri = data.getData();
                selectedImagePath = getPath(selectedImageUri);
                System.out.println("Image Path : " + selectedImagePath);
                //img.setImageURI(selectedImageUri);
            }
        }
    }

    public String getPath(Uri uri) {
        String[] projection = {MediaStore.Images.Media.DATA};
        Cursor cursor = managedQuery(uri, projection, null, null, null);
        int column_index = cursor.getColumnIndexOrThrow(MediaStore.Images.Media.DATA);
        cursor.moveToFirst();
        return cursor.getString(column_index);
    }
}
