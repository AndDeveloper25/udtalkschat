package in.moderrni.udtalkchat.activities;

import android.Manifest;
import android.annotation.TargetApi;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;

import java.util.ArrayList;
import java.util.List;

import in.moderrni.udtalkchat.R;
import in.moderrni.udtalkchat.utils.SharedPref;
import in.moderrni.udtalkchat.utils.Utility;
/*
import in.moderrni.udtalkchat.activities.R;
import in.moderrni.udtalkchat.activities.HomeActivity;
import com.udtalks.hazem.registration.activities.RegistrationMobileActivity;
import com.udtalks.hazem.utils.Constants;
import com.udtalks.hazem.utils.Utility;
*/

public class SplashScreen extends AppCompatActivity {
    private static final long SPLASH_TIME_OUT = 3000;
    private static final int PERMISSION_REQUEST_CONTACT = 0;
    String mobilenumber;//save shared preferance data
    public static final int MULTIPLE_PERMISSIONS = 10; // code you want.
    String[] permissions = new String[]{
            Manifest.permission.WRITE_EXTERNAL_STORAGE,
            Manifest.permission.CAMERA,
            Manifest.permission.SEND_SMS,
            Manifest.permission.CALL_PHONE,
            Manifest.permission.READ_CONTACTS,
            Manifest.permission.ACCESS_COARSE_LOCATION,
            Manifest.permission.ACCESS_FINE_LOCATION};

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash_screen);
        // askForPermission();

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (checkPermissions()) {

                navigateToNextActivity(mobilenumber);
            }
        } else {

            navigateToNextActivity(mobilenumber);
        }

    }

    private boolean checkPermissions() {
        int result;
        List<String> listPermissionsNeeded = new ArrayList<>();
        for (String p : permissions) {
            result = ContextCompat.checkSelfPermission(this, p);
            if (result != PackageManager.PERMISSION_GRANTED) {
                listPermissionsNeeded.add(p);
            }
        }
        if (!listPermissionsNeeded.isEmpty()) {
            ActivityCompat.requestPermissions(this, listPermissionsNeeded.toArray(new String[listPermissionsNeeded.size()]), MULTIPLE_PERMISSIONS);
            return false;
        }
        return true;
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {

        switch (requestCode) {
            case MULTIPLE_PERMISSIONS: {
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {

                    navigateToNextActivity(mobilenumber);
                } else {
                    String perStr = "";
                    for (String per : permissions) {
                        perStr += "\n" + per;
                    }
                    // permissions list of don't granted permission
                    checkPermissions();
                }
                return;
            }
        }

    }

    private void navigateToNextActivity(String mobilenumber) {
        new Handler(Looper.getMainLooper()).postDelayed(new Runnable() {
            @Override
            public void run() {
                Intent i;
                if (SharedPref.getSession(getApplicationContext()) != null) {
                    i = new Intent(SplashScreen.this, MainActivity.class);
                } else {
                    i = new Intent(SplashScreen.this, RegistrationMobileActivity.class);
                }

                startActivity(i);
                // overridePendingTransition(R.anim.slide_out_right, R.anim.slide_in_left);
                finish();
            }
        }, 3000);

    }


   /* public void showNoInternetConnection() {
        Utility.showCustomToast(SplashScreen.this, getString(R.string.no_internet));

    }*/
}