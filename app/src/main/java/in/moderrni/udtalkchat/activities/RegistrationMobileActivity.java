package in.moderrni.udtalkchat.activities;


import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.AppCompatCheckBox;
import android.text.TextUtils;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.TextView;

import com.google.gson.Gson;

import in.moderrni.udtalkchat.models.RegisterMobileModel;


import org.json.JSONException;
import org.json.JSONObject;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import in.moderrni.udtalkchat.R;
import in.moderrni.udtalkchat.interfaces.VolleyWebserviceCallBack;
import in.moderrni.udtalkchat.utils.Constants;
import in.moderrni.udtalkchat.utils.SharedPref;
import in.moderrni.udtalkchat.utils.Utility;

public class RegistrationMobileActivity extends AppCompatActivity implements VolleyWebserviceCallBack {
    @BindView(R.id.et_pNumber)
    EditText et_pNumber;
    @BindView(R.id.et_countryCode)
    TextView et_countryCode;
    @BindView(R.id.cb_tnc)
    AppCompatCheckBox cb_tnc;
    @BindView(R.id.tv_country)
    TextView tv_country;
    private String countryCode = "";
    private String countryName = "China";

    public static final String KEY = "MyPrefs" ;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_registration_mobile);
        ButterKnife.bind(this);
    }

    @OnClick({R.id.btn_continue})
    void getOtp() {

        if (TextUtils.isEmpty(countryCode)) {
            Utility.showSnackBar(et_countryCode, getResources().getString(R.string.please_select_country_code));
        } else if (et_pNumber.getText().toString().length() < 2) {
            Utility.showSnackBar(et_countryCode, getResources().getString(R.string.please_enter_mobile_number));
        } else if (!cb_tnc.isChecked()) {
            Utility.showSnackBar(et_countryCode, getResources().getString(R.string.please_accept_terms));
        } else {
            try {
                JSONObject sendData = new JSONObject();
                sendData.put("mobile_no", countryCode + et_pNumber.getText().toString());
                String url = Constants.BASE_URL + Constants.REGISTER_MOBILE;
                VolleyWebserviceCall.getInstance().postApiCall(this, url, sendData, RegistrationMobileActivity.this, true, Constants.REGISTER_MOBILE);
                Utility.printLog(Constants.REGISTER_MOBILE, sendData.toString());

            } catch (JSONException e) {
                e.printStackTrace();
            }

        }
    }

    @OnClick({R.id.tv_privacy})
    void openTerms() {
        Intent intent = new Intent(this, TermsConditions.class);
        startActivity(intent);
    }

    @OnClick({R.id.tv_terms})
    void openPolicy() {
        Intent intent = new Intent(this, TermsConditions.class);
        intent.putExtra("terms", "terms");
        startActivity(intent);
    }

    @OnClick({R.id.view_countryCode, R.id.et_countryCode})
    void openCountrySelection() {
        Intent intent = new Intent(this, SelectCountryActivity.class);
        startActivityForResult(intent, 1);
    }

    @Override
    public void onSuccessApiCall(JSONObject serverResult, String requestTag, int statusCode) {
        Utility.printLog(Constants.REGISTER_MOBILE, serverResult.toString());
        RegisterMobileModel response = new Gson().fromJson(serverResult.toString(), RegisterMobileModel.class);
        response.setCountryName(countryName);
        response.setCountryCode(countryCode);
        response.setMobileNumber(et_pNumber.getText().toString());
        if (Boolean.parseBoolean(response.getStatus())) {

            Intent intent = new Intent(this, ConfirmMobileActivity.class);
            intent.putExtra(Constants.USER_DATA, response);
            startActivity(intent);
        } else {
            Utility.showSnackBar(et_countryCode, getResources().getString(R.string.error_fetching_data));
        }
    }

    @Override
    public void onErrorApiCall(JSONObject serverResult, String requestTag, int statusCode) {

    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == 1) {
            if (resultCode == Activity.RESULT_OK) {
                countryCode = data.getStringExtra("clCode").replace(" ", "").trim();
                countryName = data.getStringExtra("countryName");
                et_countryCode.setText("(" + countryCode + ")");
                tv_country.setText(countryName);
                et_countryCode.setTextColor(getResources().getColor(R.color.black));
//                int countryFlag = data.getIntExtra("clFlagId", R.drawable.flag);
//                selectCode.setImageDrawable(this.getResources().getDrawable(countryFlag));
            }
            if (resultCode == Activity.RESULT_CANCELED) {
                //Write your code if there's no result
//                mtvCountryCode.setText("+" + countryCode);
//                countryCode =
//                selectCode.setImageDrawable(this.getResources().getDrawable(R.drawable.china));
            }
        }
    }

}
