package in.moderrni.udtalkchat.daimajia_swipe.interfaces;


import java.util.List;

import in.moderrni.udtalkchat.daimajia_swipe.Attributes;
import in.moderrni.udtalkchat.daimajia_swipe.SwipeLayout;

public interface SwipeItemMangerInterface {

    void openItem(int position);

    void closeItem(int position);

    void closeAllExcept(SwipeLayout layout);
    
    void closeAllItems();

    List<Integer> getOpenItems();

    List<SwipeLayout> getOpenLayouts();

    void removeShownLayouts(SwipeLayout layout);

    boolean isOpen(int position);

    Attributes.Mode getMode();

    void setMode(Attributes.Mode mode);
}
