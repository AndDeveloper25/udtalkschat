package in.moderrni.udtalkchat.daimajia_swipe.interfaces;

import in.moderrni.udtalkchat.daimajia_swipe.SwipeLayout;

public class SimpleSwipeListener implements SwipeLayout.SwipeListener {

    @Override
    public void onStartOpen(SwipeLayout layout) {
    }

    @Override
    public void onOpen(SwipeLayout layout) {
    }

    @Override
    public void onStartClose(SwipeLayout layout) {
    }

    @Override
    public void onClose(SwipeLayout layout) {
    }

    @Override
    public void onUpdate(SwipeLayout layout, int leftOffset, int topOffset) {
    }

    @Override
    public void onHandRelease(SwipeLayout layout, float xvel, float yvel) {
    }
}
