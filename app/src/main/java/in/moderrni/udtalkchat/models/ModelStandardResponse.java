package in.moderrni.udtalkchat.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class ModelStandardResponse {

    public ModelStandardResponse(Boolean status, Integer groupId, String filePath, String message) {
        this.status = status;
        this.groupId = groupId;
        this.filePath = filePath;
        this.message = message;
    }

    @Expose
    private Boolean status;
    @SerializedName("group_id")
    @Expose
    private Integer groupId;
    @SerializedName("file_path")
    @Expose
    private String filePath;
    @SerializedName("message")
    @Expose
    private String message;

    public Boolean getStatus() {
        return status;
    }

    public void setStatus(Boolean status) {
        this.status = status;
    }

    public Integer getGroupId() {
        return groupId;
    }

    public void setGroupId(Integer groupId) {
        this.groupId = groupId;
    }

    public String getFilePath() {
        return filePath;
    }

    public void setFilePath(String filePath) {
        this.filePath = filePath;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}
