package in.moderrni.udtalkchat.models;

import android.arch.persistence.room.ColumnInfo;
import android.arch.persistence.room.Entity;
import android.arch.persistence.room.PrimaryKey;
import android.widget.ImageView;

/*Model table for Room storage*/
@Entity
public class User {

    public User(int user_id, String nickname, String mobile_no, String typeOfUser, String products, String country, String profile_pic, String company, String profileFlag, boolean status, boolean isSelected) {
        this.user_id = user_id;
        this.nickname = nickname;
        this.mobile_no = mobile_no;
        this.typeOfUser = typeOfUser;
        this.country = country;
        this.products = products;
        this.profile_pic = profile_pic;
        this.company = company;
        this.profileFlag = profileFlag;
        this.status = status;
        this.isSelected = isSelected;

    }

    @PrimaryKey(autoGenerate = false)
    @ColumnInfo(name = "user_id")
    private int user_id;
    @ColumnInfo(name = "nickname")
    private String nickname;
    @ColumnInfo(name = "mobile_no")
    private String mobile_no;
    @ColumnInfo(name = "career")
    private String typeOfUser;
    @ColumnInfo(name = "country")
    private String country;
    @ColumnInfo(name = "products")
    private String products;
    @ColumnInfo(name = "profile_pic")
    private String profile_pic;
    @ColumnInfo(name = "company")
    private String company;
    @ColumnInfo(name = "profileFlag")
    private String profileFlag;
    @ColumnInfo(name = "status")
    private boolean status;
    @ColumnInfo(name = "isSelected")
    private boolean isSelected;

    public int getUser_id() {
        return user_id;
    }

    public void setUser_id(int user_id) {
        this.user_id = user_id;
    }

    public String getNickname() {
        return nickname;
    }

    public void setNickname(String nickname) {
        this.nickname = nickname;
    }

    public String getMobile_no() {
        return mobile_no;
    }

    public void setMobile_no(String mobile_no) {
        this.mobile_no = mobile_no;
    }

    public String getTypeOfUser() {
        return typeOfUser;
    }

    public void setTypeOfUser(String typeOfUser) {
        this.typeOfUser = typeOfUser;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public String getProducts() {
        return products;
    }

    public void setProducts(String products) {
        this.products = products;
    }

    public String getProfile_pic() {
        return profile_pic;
    }

    public void setProfile_pic(String profile_pic) {
        this.profile_pic = profile_pic;
    }

    public String getCompany() {
        return company;
    }

    public void setCompany(String company) {
        this.company = company;
    }

    public String getProfileFlag() {
        return profileFlag;
    }

    public void setProfileFlag(String profileFlag) {
        this.profileFlag = profileFlag;
    }

    public boolean isStatus() {
        return status;
    }

    public void setStatus(boolean status) {
        this.status = status;
    }

    public boolean isSelected() {
        return isSelected;
    }

    public void setSelected(boolean selected) {
        isSelected = selected;
    }
}