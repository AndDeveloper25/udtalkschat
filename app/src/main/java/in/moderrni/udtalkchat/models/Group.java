package in.moderrni.udtalkchat.models;

import android.arch.persistence.room.ColumnInfo;
import android.arch.persistence.room.Entity;
import android.arch.persistence.room.PrimaryKey;

import java.util.List;

/*Model table for Room storage*/
@Entity
public class Group {

    @PrimaryKey(autoGenerate = true)//id should be auto generated
    @ColumnInfo(name = "group_id")
    private int groupId;
    @ColumnInfo(name = "group_name")
    private String groupName;
    @ColumnInfo(name = "xmpp_group_name")
    private String xmpp_group_name;
    @ColumnInfo(name = "group_pic")
    private String group_pic;
    @ColumnInfo(name = "admin_id")
    private String admin_id;
    @ColumnInfo(name = "member_list")
    private List<User> userList;
    @ColumnInfo(name = "description")
    private String groupDescription;

    public Group(int groupId, String groupName, String xmpp_group_name, String group_pic, String admin_id, String groupDescription, List<User> userList) {
        this.groupId = groupId;
        this.groupName = groupName;
        this.xmpp_group_name = xmpp_group_name;
        this.group_pic = group_pic;
        this.admin_id = admin_id;
        this.userList = userList;
        this.groupDescription = groupDescription;
    }

    public int getGroupId() {
        return groupId;
    }

    public void setGroupId(int groupId) {
        this.groupId = groupId;
    }

    public String getGroupName() {
        return groupName;
    }

    public void setGroupName(String groupName) {
        this.groupName = groupName;
    }

    public String getXmpp_group_name() {
        return xmpp_group_name;
    }

    public void setXmpp_group_name(String xmpp_group_name) {
        this.xmpp_group_name = xmpp_group_name;
    }

    public String getGroup_pic() {
        return group_pic;
    }

    public void setGroup_pic(String group_pic) {
        this.group_pic = group_pic;
    }

    public String getAdmin_id() {
        return admin_id;
    }

    public void setAdmin_id(String admin_id) {
        this.admin_id = admin_id;
    }

    public List<User> getUserList() {
        return userList;
    }

    public void setUserList(List<User> userList) {
        this.userList = userList;
    }

    public String getGroupDescription() {
        return groupDescription;
    }

    public void setGroupDescription(String groupDescription) {
        this.groupDescription = groupDescription;
    }
}
