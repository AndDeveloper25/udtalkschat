package in.moderrni.udtalkchat.models.converters;

import android.arch.persistence.room.TypeConverter;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.lang.reflect.Type;
import java.util.Collections;
import java.util.List;

import in.moderrni.udtalkchat.models.Group;
import in.moderrni.udtalkchat.models.User;

public class Converters {

    static Gson gson = new Gson();

    @TypeConverter
    public static List<Group> stringToGroupList(String data) {
        if (data == null) {
            return Collections.emptyList();
        }

        Type listType = new TypeToken<List<Group>>() {
        }.getType();

        return gson.fromJson(data, listType);
    }

    @TypeConverter
    public static String groupListToString(List<Group> someObjects) {
        return gson.toJson(someObjects);
    }

    @TypeConverter
    public static List<User> stringToUserList(String data) {
        if (data == null) {
            return Collections.emptyList();
        }

        Type listType = new TypeToken<List<User>>() {
        }.getType();

        return gson.fromJson(data, listType);
    }

    @TypeConverter
    public static String userListToString(List<User> someObjects) {
        return gson.toJson(someObjects);
    }
}