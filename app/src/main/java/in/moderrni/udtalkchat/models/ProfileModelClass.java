
package in.moderrni.udtalkchat.models;

import android.arch.persistence.room.PrimaryKey;
import android.support.annotation.Nullable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import io.realm.RealmObject;


public class ProfileModelClass extends RealmObject {
public ProfileModelClass(){}
    @SerializedName("status")
    @Expose
    @Nullable
    private Boolean status;
    @SerializedName("privacy_status")
    @Expose
    @Nullable
    private String privacyStatus;
    @PrimaryKey
    @SerializedName("user_id")
    @Expose
    @Nullable
    private String userId;
    @SerializedName("nickname")
    @Expose
    @Nullable
    private String nickname;
    @SerializedName("mobile_no")
    @Expose
    @Nullable
    private String mobileNo;
    @SerializedName("country")
    @Expose
    @Nullable
    private String country;
    @SerializedName("career")
    @Expose
    @Nullable
    private String career;
    @SerializedName("company")
    @Expose
    @Nullable
    private String company;
    @SerializedName("products")
    @Expose
    @Nullable
    private String products;
    @SerializedName("profile_pic")
    @Expose
    @Nullable
    private String profilePic;
    @SerializedName("back_image")
    @Expose
    @Nullable
    private String backImage;
    @SerializedName("user_status")
    @Expose
    @Nullable
    private String userStatus;

    @SerializedName("street")
    @Expose
    @Nullable
    private String street;

    @SerializedName("city")
    @Expose
    @Nullable
    private String city;

    @SerializedName("gender")
    @Expose
    @Nullable
    private String gender;

    @SerializedName("dob")
    @Expose
    @Nullable
    private String dob;

    @SerializedName("mypost_thumb")
    @Expose
    @Nullable
    private ThumnailMyPost mypostThumb;

    public Boolean getStatus() {
        return status;
    }

    public void setStatus(Boolean status) {
        this.status = status;
    }

    public String getPrivacyStatus() {
        return privacyStatus;
    }

    public void setPrivacyStatus(String privacyStatus) {
        this.privacyStatus = privacyStatus;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getNickname() {
        return nickname;
    }

    public void setNickname(String nickname) {
        this.nickname = nickname;
    }

    public String getMobileNo() {
        return mobileNo;
    }

    public void setMobileNo(String mobileNo) {
        this.mobileNo = mobileNo;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public String getCareer() {
        return career;
    }

    public void setCareer(String career) {
        this.career = career;
    }

    public String getCompany() {
        return company;
    }

    public void setCompany(String company) {
        this.company = company;
    }

    public String getProducts() {
        return products;
    }

    public void setProducts(String products) {
        this.products = products;
    }

    public String getProfilePic() {
        return profilePic;
    }

    public void setProfilePic(String profilePic) {
        this.profilePic = profilePic;
    }

    public String getBackImage() {
        return backImage;
    }

    public void setBackImage(String backImage) {
        this.backImage = backImage;
    }

    public String getUserStatus() {
        return userStatus;
    }

    public void setUserStatus(String userStatus) {
        this.userStatus = userStatus;
    }

    public ThumnailMyPost getMypostThumb() {
        return mypostThumb;
    }

    public void setMypostThumb(ThumnailMyPost mypostThumb) {
        this.mypostThumb = mypostThumb;
    }

    @Nullable
    public String getStreet() {
        return street;
    }

    public void setStreet(@Nullable String street) {
        this.street = street;
    }

    @Nullable
    public String getCity() {
        return city;
    }

    public void setCity(@Nullable String city) {
        this.city = city;
    }

    @Nullable
    public String getGender() {
        return gender;
    }

    public void setGender(@Nullable String gender) {
        this.gender = gender;
    }

    @Nullable
    public String getDob() {
        return dob;
    }

    public void setDob(@Nullable String dob) {
        this.dob = dob;
    }
}
