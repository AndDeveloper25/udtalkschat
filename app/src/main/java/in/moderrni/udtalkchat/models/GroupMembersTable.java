package in.moderrni.udtalkchat.models;


import android.arch.persistence.room.ColumnInfo;
import android.arch.persistence.room.Entity;
import android.arch.persistence.room.ForeignKey;
import android.arch.persistence.room.PrimaryKey;

@Entity(foreignKeys = {@ForeignKey(entity = User.class, parentColumns = "user_id", childColumns = "user_id"),
        @ForeignKey(entity = Group.class, parentColumns = "group_id", childColumns = "group_id")})
public class GroupMembersTable {

    @PrimaryKey(autoGenerate = true)//id should be auto generated
    private int autoID;

    @ColumnInfo(name = "adminId")
    private int adminId;

    @ColumnInfo(name = "user_id")
    private int userId;

    @ColumnInfo(name = "group_id")
    private int groupId;

    public GroupMembersTable(int groupID, int adminId, int userId, int groupId) {
        this.autoID = groupID;
        this.adminId = adminId;
        this.userId = userId;
        this.groupId = groupId;
    }

    public int getGroupID() {
        return autoID;
    }

    public void setGroupID(int groupID) {
        this.autoID = groupID;
    }

    public int getAdminId() {
        return adminId;
    }

    public void setAdminId(int adminId) {
        this.adminId = adminId;
    }

    public int getUserId() {
        return userId;
    }

    public void setUserId(int userId) {
        this.userId = userId;
    }

    public int getGroupId() {
        return groupId;
    }

    public void setGroupId(int groupId) {
        this.groupId = groupId;
    }
}
