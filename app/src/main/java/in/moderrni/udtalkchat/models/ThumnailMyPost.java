
package in.moderrni.udtalkchat.models;

import android.support.annotation.Nullable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import io.realm.RealmObject;

public class ThumnailMyPost extends RealmObject {
public ThumnailMyPost(){}
    @SerializedName("image_1")
    @Expose
    @Nullable
    private String image1;
    @SerializedName("image_2")
    @Expose
    @Nullable
    private String image2;
    @SerializedName("image_3")
    @Expose
    @Nullable
    private String image3;

    public String getImage1() {
        return image1;
    }

    public void setImage1(String image1) {
        this.image1 = image1;
    }

    public String getImage2() {
        return image2;
    }

    public void setImage2(String image2) {
        this.image2 = image2;
    }

    public String getImage3() {
        return image3;
    }

    public void setImage3(String image3) {
        this.image3 = image3;
    }

}
