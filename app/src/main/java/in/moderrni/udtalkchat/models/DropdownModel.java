package in.moderrni.udtalkchat.models;

/**
 * Created by suheb on 15/6/17.
 */

public class DropdownModel {
    int icon; String title;
    public DropdownModel(int icon, String titile) {
        this.icon=icon;
        this.title=titile;
    }

    public int getIcon() {
        return icon;
    }

    public void setIcon(int icon) {
        this.icon = icon;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }
}
