
package in.moderrni.udtalkchat.models;

import android.os.Parcel;
import android.os.Parcelable;
import android.support.annotation.Nullable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;

public class UserDetail extends RealmObject implements Parcelable {
    public UserDetail() {
    }

    @PrimaryKey
    @SerializedName("user_id")
    @Expose
    @Nullable
    private String userId;
    @SerializedName("name")
    @Expose
    @Nullable
    private String name;
    @SerializedName("profile_pic")
    @Expose
    @Nullable
    private String profilePic;
    @SerializedName("country")
    @Expose
    @Nullable
    private String country;
    @SerializedName("career")
    @Expose
    @Nullable
    private String career;
    @SerializedName("products")
    @Expose
    @Nullable
    private String products;
    @SerializedName("company")
    @Expose
    @Nullable
    private String company;
    @SerializedName("user_status")
    @Expose
    @Nullable
    private String userStatus;

    protected UserDetail(Parcel in) {
        userId = in.readString();
        name = in.readString();
        profilePic = in.readString();
        country = in.readString();
        career = in.readString();
        products = in.readString();
        company = in.readString();
        userStatus = in.readString();
    }

    public static final Creator<UserDetail> CREATOR = new Creator<UserDetail>() {
        @Override
        public UserDetail createFromParcel(Parcel in) {
            return new UserDetail(in);
        }

        @Override
        public UserDetail[] newArray(int size) {
            return new UserDetail[size];
        }
    };

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getProfilePic() {
        return profilePic;
    }

    public void setProfilePic(String profilePic) {
        this.profilePic = profilePic;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public String getCareer() {
        return career;
    }

    public void setCareer(String career) {
        this.career = career;
    }

    public String getProducts() {
        return products;
    }

    public void setProducts(String products) {
        this.products = products;
    }

    public String getCompany() {
        return company;
    }

    public void setCompany(String company) {
        this.company = company;
    }

    public String getUserStatus() {
        return userStatus;
    }

    public void setUserStatus(String userStatus) {
        this.userStatus = userStatus;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(userId);
        dest.writeString(name);
        dest.writeString(profilePic);
        dest.writeString(country);
        dest.writeString(career);
        dest.writeString(products);
        dest.writeString(company);
        dest.writeString(userStatus);
    }
}
