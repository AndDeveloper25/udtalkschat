package in.moderrni.udtalkchat.models;


import android.os.Parcel;
import android.os.Parcelable;
import android.support.annotation.Nullable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;


public class RegisterMobileModel implements Parcelable{

    @SerializedName("is_register")
    @Expose
    @Nullable
    private String isRegister;
    @SerializedName("status")
    @Expose
    @Nullable
    private String status;
    @SerializedName("otp")
    @Expose
    @Nullable
    private String otp;
    @SerializedName("message")
    @Expose
    @Nullable
    private String message;
    @SerializedName("user_details")
    @Expose
    @Nullable
   private List<UserDetail> userDetails = null;

    private String mobileNumber;
    private String countryCode;

    protected RegisterMobileModel(Parcel in) {
        isRegister = in.readString();
        status = in.readString();
        otp = in.readString();
        message = in.readString();
        userDetails = in.createTypedArrayList(UserDetail.CREATOR);
        mobileNumber = in.readString();
        countryCode = in.readString();
        countryName = in.readString();
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(isRegister);
        dest.writeString(status);
        dest.writeString(otp);
        dest.writeString(message);
        dest.writeTypedList(userDetails);
        dest.writeString(mobileNumber);
        dest.writeString(countryCode);
        dest.writeString(countryName);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<RegisterMobileModel> CREATOR = new Creator<RegisterMobileModel>() {
        @Override
        public RegisterMobileModel createFromParcel(Parcel in) {
            return new RegisterMobileModel(in);
        }

        @Override
        public RegisterMobileModel[] newArray(int size) {
            return new RegisterMobileModel[size];
        }
    };

    public String getMobileNumber() {
        return mobileNumber;
    }

    public void setMobileNumber(String mobileNumber) {
        this.mobileNumber = mobileNumber;
    }

    public String getCountryCode() {
        return countryCode;
    }

    public void setCountryCode(String countryCode) {
        this.countryCode = countryCode;
    }

    public String getCountryName() {
        return countryName;
    }

    public void setCountryName(String countryName) {
        this.countryName = countryName;
    }

    private String countryName;



    public String getIsRegister() {
        return isRegister;
    }

    public void setIsRegister(String isRegister) {
        this.isRegister = isRegister;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getOtp() {
        return otp;
    }

    public void setOtp(String otp) {
        this.otp = otp;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public List<UserDetail> getUserDetails() {
        return userDetails;
    }

    public void setUserDetails(List<UserDetail> userDetails) {
        this.userDetails = userDetails;
    }


}
