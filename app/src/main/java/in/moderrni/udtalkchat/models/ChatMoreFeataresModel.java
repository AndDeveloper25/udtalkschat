package in.moderrni.udtalkchat.models;

public class ChatMoreFeataresModel {
    private String tvChatItem;
    private int ivMoreItem;

    public ChatMoreFeataresModel(String tvChatItem, int ivMoreItem) {
        this.tvChatItem = tvChatItem;
        this.ivMoreItem = ivMoreItem;
    }

    public String getTvChatItem() {
        return tvChatItem;
    }

    public void setTvChatItem(String tvChatItem) {
        this.tvChatItem = tvChatItem;
    }

    public int getIvMoreItem() {
        return ivMoreItem;
    }

    public void setIvMoreItem(int ivMoreItem) {
        this.ivMoreItem = ivMoreItem;
    }
}
