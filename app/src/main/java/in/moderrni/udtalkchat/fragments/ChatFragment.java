package in.moderrni.udtalkchat.fragments;


import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

import android.support.annotation.RequiresApi;
import android.support.constraint.ConstraintLayout;
import android.support.v4.app.Fragment;
import android.support.v7.widget.DefaultItemAnimator;

import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;

import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.PopupWindow;
import android.widget.Toast;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import in.moderrni.udtalkchat.R;

import in.moderrni.udtalkchat.activities.ActivityCreateBusinessGroup;
import in.moderrni.udtalkchat.activities.AddUDUser;
import in.moderrni.udtalkchat.activities.MainActivity;
import in.moderrni.udtalkchat.models.DropdownModel;
import in.moderrni.udtalkchat.models.User;
import in.moderrni.udtalkchat.ui.adapters.AllUsersDataAdapter;

import in.moderrni.udtalkchat.ui.adapters.CustomDropdownAdapter;
import in.moderrni.udtalkchat.utils.core.AppSingletonDatabase;
import io.reactivex.Observable;
import io.reactivex.Observer;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;


public class ChatFragment extends Fragment implements View.OnClickListener {
    View view;
    @BindView(R.id.recyclerView)
    RecyclerView recyclerView;
    @BindView(R.id.fragmentChatTab)
    View noContactWallpaper;
    private static String TAG = ChatFragment.class.getSimpleName();
    private AppSingletonDatabase appSingletonDatabase;
    LinearLayoutManager linearLayoutManager;
    AllUsersDataAdapter allUsersDataAdapter;
    Observable<List<User>> listObservable;

    @BindView(R.id.toolbar)
    android.support.constraint.ConstraintLayout toolbar;

    @BindView(R.id.ivSearch)
    ImageView ivSearch;
    @BindView(R.id.ivAdduser)
    ImageView ivAdduser;
    @BindView(R.id.ivCancalSearchBar)
    ImageView ivCancalSearchBar;
    @BindView(R.id.cnSearchBar)
    ConstraintLayout cnSearchBar;
    private PopupWindow popupDialog;
    @BindView(R.id.etSearchBar)
    EditText etsearchbar;

    //List<User> mUserList;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        createObserver();
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_chat, container, false);
        ButterKnife.bind(this, view);
        initView();
        return view;

    }


    private void initView() {
        createObserver();
        cnSearchBar.setOnClickListener(this);
        ivSearch.setOnClickListener(this);
        ivAdduser.setOnClickListener(this);
        ivCancalSearchBar.setOnClickListener(this);
        linearLayoutManager = new LinearLayoutManager(getActivity());
        recyclerView.setLayoutManager(linearLayoutManager);//set layout to recyclerview
        allUsersDataAdapter = new AllUsersDataAdapter(getActivity());//set list to recyclerview
        recyclerView.setAdapter(allUsersDataAdapter);//set adapter to recycler view
        appSingletonDatabase = AppSingletonDatabase.getINSTANCE(getActivity());//create instance of Room db
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        recyclerView.addItemDecoration(new DividerItemDecoration(getActivity(), DividerItemDecoration.VERTICAL));
        SearchResult();

    }

    private void SearchResult() {


        //adding a TextChangedListener
        //to call a method whenever there is some change on the EditText

        etsearchbar.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {


                allUsersDataAdapter.getFilter().filter(s); //call filter
            }

            @Override
            public void afterTextChanged(Editable s) {
            }
        });
    }

    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.cnSearchBar:
                Toast.makeText(getActivity(), "thndfhgdfjh", Toast.LENGTH_SHORT).show();
                break;
            case R.id.ivSearch:
                Toast.makeText(getActivity(), "seach bar", Toast.LENGTH_SHORT).show();
                cnSearchBar.setVisibility(View.VISIBLE);
                break;
            case R.id.ivAdduser:
                OpenUserAddDialoge();
                Toast.makeText(getActivity(), "add user", Toast.LENGTH_SHORT).show();
                break;
            case R.id.ivCancalSearchBar:
                cnSearchBar.setVisibility(View.GONE);
                break;
        }
    }

    private void createObserver() {
        listObservable = Observable.just(getAllUsers());
        listObservable
                .subscribeOn(Schedulers.newThread())//Observable runs on new background thread. method specifies thread on which onSubscribe method will be executed.
                .observeOn(AndroidSchedulers.mainThread())//Observer will run on main UI thread. method specifies thread on which onNext/onError/onCompleted will be executed
                .subscribe(new Observer<List<User>>() {

                    @Override
                    public void onSubscribe(Disposable d) {
                        // d.dispose();//dispose the subscription
                    }

                    @Override
                    public void onNext(List<User> usersList) {
                        //here will get the actual list so add it in Room databse
                        if (usersList.size() > 1) { //why 1 becoz already one static static item by default added in list
                            // Log.d(TAG, "onNext: " + usersList.get(1).getCountryName());
                            noContactWallpaper.setVisibility(View.GONE);
                            setListToRoom(usersList);
                        } else if (usersList.size() == 1) {
                            noContactWallpaper.setVisibility(View.VISIBLE);
                            setListToRoom(usersList);
                        } else
                            noContactWallpaper.setVisibility(View.GONE);

                    }

                    @Override
                    public void onError(Throwable e) {
                        Log.d(TAG, "onError: " + e.getMessage());

                    }

                    @Override
                    public void onComplete() {
                        // allUsersDataAdapter.notifyDataSetChanged();
                    }
                });
    }


    private void setListToRoom(List<User> usersList) {
        appSingletonDatabase.userDao().deleteAllUsers();
        appSingletonDatabase.userDao().insertAllUsers(usersList);//insert received data in Room
        allUsersDataAdapter.setList(appSingletonDatabase.userDao().getAllUsers(1));
        allUsersDataAdapter.notifyDataSetChanged();
    }


    /*this is static data*/
    private List<User> getAllUsers() {
        List<User> getUserList = new ArrayList<>();

        getUserList.add(new User(1, "UDTalks", "News", "News", "Welcome message and quick tips on how to use UDTalks", "Egypt", "http:\\/\\/www.ebitcoinics.asia\\/app\\/uploads\\/currency\\/currency-malaysia.png", "LnT", "news", true, false));
        getUserList.add(new User(2, "Dev Group", "", "Buyer", "New way to meet for business perspective", "Group", "http:\\/\\/www.ebitcoinics.asia\\/app\\/uploads\\/currency\\/currency-malaysia.png", "", "group", true, false));
        getUserList.add(new User(3, "Kiran", "9637981895", "Friend", "mobile,batteries", "Egypt", "http:\\/\\/www.ebitcoinics.asia\\/app\\/uploads\\/currency\\/currency-malaysia.png", "Modern I Infotech", "user", true, false));
        getUserList.add(new User(4, "Harshal", "9637981895", "Seller", "mobile,batteries", "United Kingdom", "http:\\/\\/www.ebitcoinics.asia\\/app\\/uploads\\/currency\\/currency-malaysia.png", "Accenture", "user", true, false));
        getUserList.add(new User(5, "Business Group", "", "Buyer", "New way to meet for business perspective", "Group", "http:\\/\\/www.ebitcoinics.asia\\/app\\/uploads\\/currency\\/currency-malaysia.png", "", "group", true, false));
        getUserList.add(new User(6, "Nupur", "9637981895", "Agent", "mobile,batteries", "Egypt", "http:\\/\\/www.ebitcoinics.asia\\/app\\/uploads\\/currency\\/currency-malaysia.png", "Modern I Infotech", "user", true, false));
        getUserList.add(new User(7, "Priyanka", "9637981895", "Buyer", "mobile,batteries", "United Kingdom", "http:\\/\\/www.ebitcoinics.asia\\/app\\/uploads\\/currency\\/currency-malaysia.png", "Accenture", "user", true, false));
        getUserList.add(new User(9, "New Treands of Business", "", "Buyer", "New way to meet for business perspective", "Group", "http:\\/\\/www.ebitcoinics.asia\\/app\\/uploads\\/currency\\/currency-malaysia.png", "", "group", true, false));
        getUserList.add(new User(10, "Deepak", "9637981895", "Friend", "mobile,batteries", "Egypt", "http:\\/\\/www.ebitcoinics.asia\\/app\\/uploads\\/currency\\/currency-malaysia.png", "Modern I Infotech", "user", true, false));


        //add these data to observable
        return getUserList;
    }

    @Override
    public void onStart() {
        super.onStart();
        initView();

    }

    @Override
    public void onDestroy() {
        super.onDestroy();

    }


    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    private void OpenUserAddDialoge() {
//        android.support.constraint.ConstraintLayout viewGroup = (android.support.constraint.ConstraintLayout)findViewById(R.id.mainactivitylayout_id) ;
        LayoutInflater inflater = LayoutInflater.from(getActivity());
        View layout = inflater.inflate(R.layout.layout_dropdown_adduser, null);
        popupDialog = new PopupWindow();
        popupDialog.setContentView(layout);
        popupDialog.setWidth(WindowManager.LayoutParams.MATCH_PARENT);
        popupDialog.setHeight(WindowManager.LayoutParams.MATCH_PARENT);
        popupDialog.setFocusable(true);
        popupDialog.setBackgroundDrawable(null);//for removing borders and margins
        popupDialog.showAsDropDown(toolbar);
//        popupDialog.setAnimationStyle(R.style.Animation);
        ListView listPopup1 = (ListView) layout.findViewById(R.id.listPopup);
        ArrayList<DropdownModel> al = new ArrayList<>();
        al.add(new DropdownModel(R.drawable.ic_usergroup, getResources().getString(R.string.create_business_group)));
        al.add(new DropdownModel(R.drawable.ic_group, getResources().getString(R.string.add_ud_users)));
        al.add(new DropdownModel(R.drawable.ic_phonebook, getResources().getString(R.string.add_phone_contacts)));
        al.add(new DropdownModel(R.drawable.ic_scanner, getResources().getString(R.string.scan_to_add_user)));
        View popup_background = layout.findViewById(R.id.popup_background);
        popup_background.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                popupDialog.dismiss();
            }
        });
        CustomDropdownAdapter adapter = new CustomDropdownAdapter(getActivity(), al, false);
        listPopup1.setAdapter(adapter);
        listPopup1.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                switch (position) {
                    case 0:
                        Intent intent = new Intent(getActivity(), ActivityCreateBusinessGroup.class);
                        startActivity(intent);
                        Toast.makeText(getActivity(), "create_business_group", Toast.LENGTH_SHORT).show();
                        break;
                    case 1:

                        startActivity(new Intent(getActivity(), AddUDUser.class));


                        Toast.makeText(getActivity(), "add_ud_users", Toast.LENGTH_SHORT).show();

                        break;
                    case 2:
                        Toast.makeText(getActivity(), "add_phone_contacts", Toast.LENGTH_SHORT).show();


                        break;
                    case 3:
//
                        Toast.makeText(getActivity(), "scan_to_add_user", Toast.LENGTH_SHORT).show();
                        break;
                }
                popupDialog.dismiss();
            }

        });
        popupDialog.setOnDismissListener(new PopupWindow.OnDismissListener() {
            @Override
            public void onDismiss() {
                // iv_add_user.setImageResource(R.drawable.ic_add_dots);
            }
        });


    }

}
