package in.moderrni.udtalkchat.utils.core;

import android.arch.persistence.room.Database;
import android.arch.persistence.room.Room;
import android.arch.persistence.room.RoomDatabase;
import android.content.Context;

import in.moderrni.udtalkchat.interfaces.UserDao;
import in.moderrni.udtalkchat.models.User;
import in.moderrni.udtalkchat.utils.Constants;

/*Single ton instance of Room db*/
@Database(entities = {User.class}, version = 1)
public abstract class AppSingletonDatabase extends RoomDatabase {
    public abstract UserDao userDao();

    private static AppSingletonDatabase INSTANCE = null;

    public static AppSingletonDatabase getINSTANCE(final Context context) {
        if (INSTANCE == null) {
            synchronized (AppSingletonDatabase.class) {
                if (INSTANCE == null) {
                    INSTANCE = Room.databaseBuilder(context, AppSingletonDatabase.class, Constants.DBNAME).allowMainThreadQueries().build();
                }
            }
        }
        return INSTANCE;
    }
}
