package in.moderrni.udtalkchat.utils;


import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;


public class SharedPref {

    private static final String UDTALKDB = "UDTALKDB";

    private static final String USER_ID = "user_id";


    public static boolean logoutUser(Context context) {
        // Clearing all data from Shared Preferences
        SharedPreferences.Editor neverDelEditor = getSharedPref(context).edit();
        neverDelEditor.clear();

        SharedPreferences.Editor firebaseEditor = getSharedPref(context).edit();
        firebaseEditor.clear();
        // After logout redirect user to Loing Activity
        //Intent i = new Intent(context, ActivityMobile.class);
        return neverDelEditor.commit();
    }


    private static SharedPreferences getSharedPref(Context context) {
        return context.getSharedPreferences(Constants.SESSION, Context.MODE_PRIVATE);
    }

    public static String getSession(Context context) {
        return getSharedPref(context).getString(Constants.MOBILE, null);
    }

    public static boolean setSession(Context context, String img) {
        SharedPreferences.Editor neverDelEditor = getSharedPref(context).edit();
        neverDelEditor.putString(Constants.MOBILE, img);
        return neverDelEditor.commit();
    }


}
