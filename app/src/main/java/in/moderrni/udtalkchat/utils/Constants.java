package in.moderrni.udtalkchat.utils;

public class Constants {
    public static final String DBNAME = "UDTalkDB";//Room db name
    public static final int PERMISSIONS_REQUEST_PHONE_CALL = 111;


    public static final String BASE_URL = "http://128.199.234.38/udtalks/api/";


    public static final String ACTIVATE_USER = "activate_user.php";

    public final static String COUNTRIES = "countries";
    public final static String SESSION = "session";
    public final static String COUNTRY_CODE_1 = "countryCode";
    public final static String COUNTRY_CODE = "+91";
    public final static String COUNTRY_NAME = "countryName";
    public final static String COUNTRY_FLAG = "flag";
    public static final String CUSTOM_STATUS = "givenName";
    public static final String USER_ID = "user_id";
    public static final String status = "status";
    public static final String NICKNAME = "nickname";
    public static final String MOBILE = "mobile";
    public static final String USER_DATA = "userdata";
    public static final String REGISTER_MOBILE = "register_mobile.php";
    public static final String IS_LOGIN = "islogin";
    public static final String THEME = "theme";
    public static final String GET_CHAT_LIST = "get_added_users.php";
    public static final String GET_USER_PROFILE_URL = "get_user_details.php";
    public static final String SEARCH_UD_USERS = "get_search_temp.php";
    public static final String ADD_USER_TO_CONTACT = "added_to_userlist.php";
    public static final String DELETE_USER_CONTACT = "delete_user.php";
    public static final String BLOCK_CONTACT_URL = "block_user.php";
    public static final String UNBLOCK_URL = "unblock_user.php";
    public static final String PATH_UD_FOLDER = "UDtalksnew";
    public static final String PATH_THEMES = PATH_UD_FOLDER + "/wallpapers";
    public static final String OFFSET = "offset";
    public static final String LIMIT = "limit";
    public static final String GET_DEALS = "get_deals.php";
    public static final String POST_TYPE = "post_type";
    public static final String SELLPOST = "sellpost";
    public static final String MOMENTS = "moment";
    public static final String BUYPOST = "buypost";
    public static final String POST_ID = "post_id";
    public static final String SELLPOST_DETAILS = "get_sell_post_new.php";
    public static final String BUYPOST_DETAILS = "get_buy_post_new.php";
    public static final String IMAGE_URL = "image_url";


    public static final String SELL_POST_URL = "sell_post_new.php";
    public static final String BUY_POST_URL = "buy_post_new.php";
    public static final String MOMENTS_URL = "moment_new.php";
    public static final String MOMENT_DETAILS = "get_moment_new.php";
    public static final String LIKE_POST = "hit_like.php";
    public static final String PIC_BASE_URL = "http://128.199.234.38/udtalks/api/profile_pics/";
    public static final String PROFILE_PIC = "pic";
    public static final String COMPANY = "company";
    public static final String GET_COMMENTS_URL = "get_comments.php";
    public static final String POST_COMMENT_URL = "comment.php";
    public static final String API_DELETE_COMMENT = "delete_comment.php";
    public static final String DOB = "dob";
    public static final String ADDRESS = "address";
    public static final String GENDER = "gender";
    public static final String MY_POST_URL = "get_mypost.php";
    public static final String GET_LIKES="get_likes.php";


    public static final String CHAT_TYPE_TEXT = "text";
    public static final String CHAT_TYPE_IMAGE = "image";
    public static final String CHAT_TYPE_VIDEO = "video";
    public static final String CHAT_TYPE_AUDIO = "audio";
    public static final String MESSAGE_ID = "mid";
    public static final String MESSAGE_SENT = "message_sent";
    public static final int MESSAGE_STATUS_PENDING = 0;
    public static final int MESSAGE_STATUS_SENT = 1;
    public static final int MESSAGE_STATUS_DELIVER = 2;
    public static final int MESSAGE_STATUS_INCOMING = 3;
    public static final Integer MESSAGE_STATUS_CANCELLED = 4;
    public static final String MESSAGE_BROADCAST = "com.sendchat.message";
    public static final String IS_OFFLINE = "isoffline";
    public static final String CANCELLED = "cancelled";
    public static final String USERJSON = "userjson";
    public static final String UPDATE_LIST = "updatelist";
    public static final String XMPP_PASSWORD = "12345";
    public static final String LIKE_TEXT = "liketext";
    public static final String IS_LIKE = "islike";
    public static final String LIKE_COUNT = "countlike";
    public static final String API_EDIT_PROFILE = "edit_user_privacy.php";
    public static final String SHARETODEALS = "share_post.php";
    public static final String HIDE_POSTS = "hide_user.php";
    public static final String REPORT_USER = "report.php";
    public static final String HIDDEN_POSTS_LIST = "get_hideList.php";
    public static final String PROFILE_ID = "profile_id";
}
