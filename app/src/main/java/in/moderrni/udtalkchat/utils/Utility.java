package in.moderrni.udtalkchat.utils;


import android.Manifest;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.ContextWrapper;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.os.Build;
import android.preference.PreferenceManager;

import android.provider.ContactsContract;
import android.support.design.widget.Snackbar;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;
import android.util.Base64;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.google.gson.Gson;


import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.Locale;

import in.moderrni.udtalkchat.R;
import in.moderrni.udtalkchat.activities.RegistrationMobileActivity;
import in.moderrni.udtalkchat.models.ProfileModelClass;


/**
 * Created by suheb on 24/5/17.
 */

public class Utility {


    public static String encodeTobase64(Bitmap bitmap) {
        try {
//            bitmap = Bitmap.createScaledBitmap(bitmap, 200, 200, false);
            ByteArrayOutputStream baos = new ByteArrayOutputStream();
            bitmap.compress(Bitmap.CompressFormat.PNG, 80, baos);
            byte[] byteArray = baos.toByteArray();
            String imageEncoded = Base64.encodeToString(byteArray, Base64.DEFAULT);

            //     Log.e("Base 64 String", imageEncoded);
            return imageEncoded;
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    public static boolean isOnline(Context context) {
        ConnectivityManager cm = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo netInfo = cm.getActiveNetworkInfo();
        //should check null because in airplane mode it will be null
        return (netInfo != null && netInfo.isConnected());
    }



    //ide soft keb on outside click
    public static void setupUI(View view, final Context context) {

        // Set up touch listener for non-text box views to hide keyboard.
        if (!(view instanceof EditText)) {
            view.setOnTouchListener(new View.OnTouchListener() {

                public boolean onTouch(View v, MotionEvent event) {
                    hideSoftKeyboard(context);
                    return false;
                }
            });
        }

        //If a layout container, iterate over children and seed recursion.
        if (view instanceof ViewGroup) {
            for (int i = 0; i < ((ViewGroup) view).getChildCount(); i++) {
                View innerView = ((ViewGroup) view).getChildAt(i);
                setupUI(innerView, context);
            }
        }
    }

    public static void hideSoftKeyboard(Context context) {

        InputMethodManager inputMethodManager =
                (InputMethodManager) context.getSystemService(
                        Context.INPUT_METHOD_SERVICE);
        if (inputMethodManager != null) {

            try {
                inputMethodManager.hideSoftInputFromWindow(((AppCompatActivity) context).getCurrentFocus().getWindowToken(), 0);
            } catch (Exception e) {
            }


        }
        //inputMethodManager.toggleSoftInput(InputMethodManager.SHOW_FORCED, 0);
    }





    //
//    /**
//     * for getting the data which stored in the shared preference
//     *
//     * @param context
//     * @param key
//     * @return
//     */
    public static String getSharedPreference(Context context, String key) {
        SharedPreferences prefs = PreferenceManager
                .getDefaultSharedPreferences(context);
        String prefData = prefs.getString(key, null);

        return prefData;

    }

//    /**
//     * For Storing the data into SharedPreference
//     *
//     * @param context
//     * @param key
//     * @param value
//     */
    public static void setSharedPreference(Context context, String key,
                                           String value) {
        try {
            SharedPreferences prefs = PreferenceManager
                    .getDefaultSharedPreferences(context);
            SharedPreferences.Editor prefsEditor = prefs.edit();
            prefsEditor.putString(key, value);
            prefsEditor.commit();
        } catch (Exception e) {

        }
    }

    public static void showSnackBar(View view, String message) {
        Snackbar.make(view, message, Snackbar.LENGTH_LONG).show();
    }

    public static boolean isEmptyString(String string) {
        if (TextUtils.isEmpty(string)) {
            return true;
        }
        return false;
    }

    public static void printLog(String tag, String message) {
        Log.d(tag, message);
    }

    public static void saveFile(File file, ByteArrayOutputStream bytes) {
        FileOutputStream fo;
        try {
            file.createNewFile();
            fo = new FileOutputStream(file);
            fo.write(bytes.toByteArray());
            fo.close();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public static void showCustomToast(Context splashScreen, String string) {
        Toast.makeText(splashScreen, "" + string, Toast.LENGTH_SHORT).show();
    }


    /**
     * method to check internet connectivity
     *
     * @param context
     * @return
     */
    public static boolean isNetworkConnected(Context context) {
        ConnectivityManager cm = (ConnectivityManager) context
                .getSystemService(Context.CONNECTIVITY_SERVICE);

        NetworkInfo netinfo = cm.getActiveNetworkInfo();

        if (netinfo != null && netinfo.isConnectedOrConnecting()) {
            NetworkInfo wifi = cm
                    .getNetworkInfo(ConnectivityManager.TYPE_WIFI);
            NetworkInfo mobile = cm
                    .getNetworkInfo(ConnectivityManager.TYPE_MOBILE);

            if ((mobile != null && mobile.isConnectedOrConnecting())
                    || (wifi != null && wifi.isConnectedOrConnecting()))
                return true;
            else
                return false;
        } else
            return false;
    }

    /**
     * method toconvert bitmap to base64
     *
     * @param image
     * @param compressFormat
     * @param quality
     * @return
     */
    public String encodeToBase64(Bitmap image, Bitmap.CompressFormat compressFormat, int quality) {
        ByteArrayOutputStream byteArrayOS = new ByteArrayOutputStream();
        image.compress(compressFormat, quality, byteArrayOS);
        return Base64.encodeToString(byteArrayOS.toByteArray(), Base64.DEFAULT);
    }

    /**
     * method to convert base64 to bitmap
     *
     * @param input
     * @return
     */
    public Bitmap decodeBase64(String input) {
        byte[] decodedBytes = Base64.decode(input, 0);
        return BitmapFactory.decodeByteArray(decodedBytes, 0, decodedBytes.length);
    }

    /**
     * method to store image from url in internal storage space
     *
     * @param bitmapImage
     * @param name
     * @param context
     * @return
     */
    public String saveToInternalStorage(Bitmap bitmapImage, String name, Context context) {
        ContextWrapper cw = new ContextWrapper(context);
        File directory = cw.getDir("chatAppStorage", Context.MODE_PRIVATE);
        File mypath = new File(directory, name + ".png");
        FileOutputStream fos = null;
        try {
            fos = new FileOutputStream(mypath);
            bitmapImage = Bitmap.createScaledBitmap(bitmapImage, 150, 150, false);
            bitmapImage.compress(Bitmap.CompressFormat.JPEG, 100, fos);
            fos.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return directory.getAbsolutePath();
    }

    /**
     * method to get image from internal storagespace
     *
     * @param context
     * @param fileName
     * @return
     */
    public Bitmap loadImageFromStorage(Context context, String fileName) {

        Bitmap b = null;
        try {
            ContextWrapper cw = new ContextWrapper(context);
            File directory = cw.getDir("chatAppStorage", Context.MODE_PRIVATE);
            String[] list = directory.list();
            ArrayList<String> stringList = new ArrayList<String>(Arrays.asList(list));

            File mypath = new File(directory, fileName + ".png");
            try {
                b = BitmapFactory.decodeStream(new FileInputStream(mypath));
                //b.compress(Bitmap.CompressFormat.JPEG,80,new ByteArrayOutputStream());
            } catch (FileNotFoundException e) {
                //e.printStackTrace();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return b;
    }


    /**
     * method to create custom xml message
     *
     * @param mID
     * @param Reciver
     * @param Sender
     * @param body
     * @return
     */
    public static String createMessage(Context context, String mID, String Reciver, String Sender, String body) {
        return "<message id=\"" + mID + "\" to=\"" + Reciver + "\" type=\"" + "chat" + "\" from=\"" + Sender + "\">" +
                "    <body>" + body + "</body>  </message>";
    }


    /**
     * method to convert timestamp to date
     *
     * @param timestamp
     * @return
     */
    public static String timestampToDate(long timestamp) {

        Date date = new Date(timestamp);
        Calendar cal = new GregorianCalendar();
        SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
        sdf.setCalendar(cal);
        cal.setTime(date);
        return sdf.format(date);

    }

    public Bitmap drawableToBitmap(Drawable drawable) {


        if (drawable instanceof BitmapDrawable) {
            return ((BitmapDrawable) drawable).getBitmap();
        }

        Bitmap bitmap = Bitmap.createBitmap(drawable.getIntrinsicWidth(),
                drawable.getIntrinsicHeight(), Bitmap.Config.ARGB_8888);
        Canvas canvas = new Canvas(bitmap);
        drawable.setBounds(0, 0, canvas.getWidth(), canvas.getHeight());
        drawable.draw(canvas);

        return bitmap;
    }


    public static void showCustomToast(Activity context, String msg) {
//        LayoutInflater inflater = context.getLayoutInflater();
//        View v = inflater.inflate(R.layout.cust_toast, null);
//        TextView msgTv = (TextView) v.findViewById(R.id.toastTv);
//        msgTv.setText("" + msg);
//        Toast toast = new Toast(context);
//        toast.setDuration(Toast.LENGTH_SHORT);
//        toast.setView(v);
//        toast.setGravity(Gravity.CENTER_VERTICAL, 0, 0);
//        toast.show();

        Toast.makeText(context, "" + msg, Toast.LENGTH_SHORT).show();

    }


    public static String getValueFromDate(String inputDate, String inputformat, String requiredformat) {
        Date date = null;
        try {
            inputDate = inputDate.trim();
            date = new SimpleDateFormat(inputformat).parse(inputDate);
            return new SimpleDateFormat(requiredformat, Locale.ENGLISH).format(date);
        } catch (Exception e) {
            //e.printStackTrace();
            //Utility.printLog("exception", "exception in parsing date: ");
            return "";
        }
    }

    public static void dialNumber(final Context mContext, boolean isBlock, boolean isPrivate, final String number) {
        if (isBlock) {
            Toast.makeText(mContext, "Please unblock user before calling", Toast.LENGTH_SHORT).show();
        } else if (isPrivate) {
            final AlertDialog.Builder dialog = new AlertDialog.Builder(mContext);
            dialog.setMessage(mContext.getString(R.string.private_alert));

            dialog.setNegativeButton("OK", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialogInterface, int i) {
                }
            });
            dialog.create();
            dialog.show();

        } else {

            final AlertDialog.Builder dialog = new AlertDialog.Builder(mContext);
            dialog.setMessage(" This is not free call..UDchat will use your local mobile network to start this call");
            dialog.setPositiveButton("Continue", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialogInterface, int i) {
                    Intent callIntent = new Intent(Intent.ACTION_CALL);
//
                    int currentapiVersion = Build.VERSION.SDK_INT;
                    if (currentapiVersion >= Build.VERSION_CODES.LOLLIPOP) {
                        callIntent.setPackage("com.android.server.telecom");
                    } else {
                        callIntent.setPackage("com.android.phone");
                    }

                    callIntent.setData(Uri.parse("tel:" + "00" + number));
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                        if (ActivityCompat.checkSelfPermission(mContext, Manifest.permission.CALL_PHONE) != PackageManager.PERMISSION_GRANTED) {
                            ((Activity) mContext).requestPermissions(new String[]{Manifest.permission.CALL_PHONE}, Constants.PERMISSIONS_REQUEST_PHONE_CALL);
                            return;
                        } else {
                            mContext.startActivity(callIntent);
                        }
                    } else {
                        mContext.startActivity(callIntent);
                    }
                }
            });
            dialog.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialogInterface, int i) {

                }
            });
            dialog.create();
            dialog.show();


        }
    }

  /*  public static void showFullPic(Context context, String profilePic) {
        Intent i = new Intent(context, ViewFullImageFromUrl.class);
        if (null != profilePic && !profilePic.isEmpty()) {
            String url = profilePic.contains("http://128.199.234.38/udtalks/api/profile_pics/") ? profilePic : "http://128.199.234.38/udtalks/api/profile_pics/" + profilePic;
            i.putExtra(Constants.IMAGE_URL, url);
        }
        context.startActivity(i);
    }
*/
    public static String getString(Context context, int please_enter_title) {
        return context.getResources().getString(please_enter_title);
    }

    public static String getTimeFromMillisecond(long yourmilliseconds) {
        SimpleDateFormat sdf = new SimpleDateFormat("HH:mm");
        Date resultdate = new Date(yourmilliseconds);
        return sdf.format(resultdate);
    }

    public static void viewProfile(Context mContext, String userId) {
        Toast.makeText(mContext, "ProfileActivity", Toast.LENGTH_SHORT).show();
        /*Intent intent = new Intent(mContext, ProfileActivity.class);
        intent.putExtra(Constants.USER_ID, userId);
        mContext.startActivity(intent);
   */ }

    public static Bitmap getBitmapFromBase64(String base_image1) {


        byte[] decodedString = Base64.decode(base_image1, Base64.DEFAULT);
        Bitmap profileImageInBitmap = BitmapFactory.decodeByteArray(
                decodedString, 0, decodedString.length);
        return profileImageInBitmap;
    }

    public static void logOut(final Context mContext) {
        final AlertDialog.Builder dialog = new AlertDialog.Builder(mContext);
        dialog.setMessage(" Are You Sure You Want To Logout?");
        dialog.setPositiveButton("Logout", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
//                Utility.setSharedPreference(mContext, Constants.IS_LOGIN, "");
//                Utility.setSharedPreference(mContext, Constants.IS_LOGIN, "");
                mContext.startActivity(new Intent(mContext, RegistrationMobileActivity.class));
                ((Activity) mContext).finish();
            }
        });
        dialog.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {

            }
        });
        dialog.create();
        dialog.show();
    }

   /* public static ProfileModelClass getUserDetails(Context context) {
        RealmResults<ProfileModelClass> details = DBManager.getInstance(context).getDataByClass(ProfileModelClass.class);
        return details.get(0);
    }
*/
    public static String getUniqueId(String senderNumber, String receiverNumber) {
        return senderNumber.compareTo(receiverNumber) < 0 ? senderNumber.split("@")[0] + receiverNumber.split("@")[0] :
                receiverNumber.split("@")[0] + senderNumber.split("@")[0];
    }

//    public static void saveUserJsonObj(ProfileModelClass userDetail, Context context) {
//        setSharedPreference(context, Constants.USERJSON, new Gson().toJson(userDetail).toString());
//    }
//    public static String getUserJsonObj(Context context) {
//        return getSharedPreference(context, Constants.USERJSON);
//    }

    public static String getLikeText(Boolean isLike, int likeCount) {
        if (isLike) {
            if (likeCount == 1) {
                return "You like this";
            } else if (likeCount == 2) {
                return "You and 1 other like this";
            } else if (likeCount > 2) {
                return "You and " + (likeCount - 1) + " others like this";
            }
        } else {
            if (likeCount == 0) {
                return "No likes";
            } else if (likeCount > 0) {
                return likeCount + " likes";
            }
        }
        return likeCount + " Likes";

        /*if (isLike) {
            likeicon.setImageResource(R.drawable.ic_like);
            if (likeCount == 1) {
                tvLikesText.setText("You like this");
            } else if (likeCount == 2) {
                tvLikesText.setText("You and 1 other like this");
            } else if (likeCount > 2) {
                tvLikesText.setText("You and " + (likeCount - 1) + " others like this");
            }
        } else {
            likeicon.setImageResource(R.drawable.unlike_icon);

            if (likeCount == 0) {
                tvLikesText.setText("No likes");
            } else if (likeCount > 0) {
                tvLikesText.setText(likeCount + " likes");
            }
        }*/
    }

   /* public static Boolean isSameUser(Context context, String userId) {
        return userId.equalsIgnoreCase(Utility.getSharedPreference(context, USER_ID));
    }
*/
    public static Bitmap compressedBitmap(String uri) {
        BitmapFactory.Options bmOptions = new BitmapFactory.Options();
        bmOptions.inJustDecodeBounds = true;
        BitmapFactory.decodeFile(uri, bmOptions);
        int photoW = bmOptions.outWidth;
        int photoH = bmOptions.outHeight;
        int targetW = 300/*mImageView.getWidth()*/;
        int targetH = 200/*mImageView.getHeight()*/;
        // Determine how much to scale down the image
        int scaleFactor = Math.min(photoW / targetW, photoH / targetH);
        // Decode the image file into a Bitmap sized to fill the View
        bmOptions.inJustDecodeBounds = false;
        bmOptions.inPurgeable = true;
        bmOptions.inSampleSize = scaleFactor;
        Log.i("compress_scale_factor", "" + scaleFactor);
        Bitmap bitmap = BitmapFactory.decodeFile(uri/*.replace("external_files", "storage/emulated/0").replace("/raw//", "")*/, bmOptions);
//        bitmap = BlurBuilder.blur(this, bitmap);

        return bitmap;
    }

    public static BitmapFactory.Options resizeImage(String uri, int width, int height) {
        BitmapFactory.Options bmOptions = new BitmapFactory.Options();
        bmOptions.inJustDecodeBounds = true;
        BitmapFactory.decodeFile(uri, bmOptions);
        int photoW = bmOptions.outWidth;
        int photoH = bmOptions.outHeight;
        int targetW = width/*mImageView.getWidth()*/;
        int targetH = height/*mImageView.getHeight()*/;
        // Determine how much to scale down the image
        int scaleFactor = Math.min(photoW / targetW, photoH / targetH);
        // Decode the image file into a Bitmap sized to fill the View
        bmOptions.inJustDecodeBounds = false;
        bmOptions.inPurgeable = true;
        bmOptions.inSampleSize = scaleFactor;
        return bmOptions;
    }

    public static void loadUsingGlide(Context context, String url, ImageView imageView, int user_placeholder) {
        if (user_placeholder == 0) {
            Glide.with(context).load(url)   //just set override like this
                    .into(imageView);
        } else {
           // Glide.with(context).load(url)   .error(user_placeholder).placeholder(user_placeholder).into(imageView);
        }
//        Glide.with(context)
//                .load(url) .placeholder(user_placeholder)
//                .listener(new RequestListener<String, GlideDrawable>() {
//                    @Override
//                    public boolean onException(Exception e, String model, Target<GlideDrawable> target, boolean isFirstResource) {
//                        return false;
//                    }
//
//                    @Override
//                    public boolean onResourceReady(GlideDrawable resource, String model, Target<GlideDrawable> target, boolean isFromMemoryCache, boolean isFirstResource) {
//                        return false;
//                    }
//                })
//                .into(imageView);


    }

}
