package in.moderrni.udtalkchat.utils;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.graphics.drawable.ColorDrawable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;


import in.moderrni.udtalkchat.R;

/**
 * Created by suheb on 10/6/17.
 */

public class Dialogs {
    private Dialogs() {
    }

    public static Dialogs dialog;

    public static synchronized Dialogs getInstance() {
        if (null == dialog) {
            dialog = new Dialogs();
        }
        return dialog;
    }

    public void showAlert(Context context, String title, String message) {
        final AlertDialog.Builder builder = new AlertDialog.Builder(context);
        // Get the layout inflater
        LayoutInflater inflater = LayoutInflater.from(context);
        builder.setMessage(message)
        // Inflate and set the layout for the dialog
        // Pass null as the parent view because its going in the dialog layout
//        builder.setView(inflater.inflate(R.layout.custom_dialog, null))
                // Add action buttons
                .setPositiveButton("ok", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int id) {

                    }
                });
        builder.create();
        builder.show();

    }

    public Dialog showCircleLoading(Context context) {
//       View v = LayoutInflater.from(context).inflate(R.layout.volley_progress_dialog, null);
        Dialog addCommentDialog = new Dialog(context);
        addCommentDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        addCommentDialog.setContentView(R.layout.volley_progress_dialog);
        addCommentDialog.getWindow().setLayout(ViewGroup.LayoutParams.MATCH_PARENT,
                ViewGroup.LayoutParams.MATCH_PARENT);
        addCommentDialog.setCancelable(true);
        addCommentDialog.setCanceledOnTouchOutside(true);
        addCommentDialog.getWindow().setBackgroundDrawable(new ColorDrawable(0));
        // initialiseViewsInDialog(addCommentDialog);
      // v.findViewById(R.id.pb_loading);
//        addCommentDialog.show();
        return addCommentDialog;
    }

}
