package in.moderrni.udtalkchat.utils;

import android.app.Application;
import android.text.TextUtils;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.toolbox.Volley;

import com.bumptech.glide.request.target.ViewTarget;

import in.moderrni.udtalkchat.R;
import in.moderrni.udtalkchat.receiver.ConnectivityReceiver;

public class UDTalkApplication extends Application {
    private static UDTalkApplication mInstance;
    public static final String TAG = UDTalkApplication.class.getSimpleName();
    private RequestQueue mRequestQueue;


    @Override
    public void onCreate() {
        super.onCreate();
        ViewTarget.setTagId(R.id.glide_tag);//Error “You must not call setTag() on a view Glide is targeting” when use Glide
        mInstance = this;
    }
/*create singleton instance of connectivity receiver*/
    public void setConnectivityListener(ConnectivityReceiver.ConnectivityReceiverListener listener) {
        ConnectivityReceiver.connectivityReceiverListener = listener;
    }

    public static synchronized UDTalkApplication getInstance() {
        return mInstance;
    }
    public <T> void addToRequestQueue(Request<T> req, String tag) {
        // set the default tag if tag is empty
        req.setTag(TextUtils.isEmpty(tag) ? TAG : tag);
        getRequestQueue().add(req);
    }
    public RequestQueue getRequestQueue() {
        if (mRequestQueue == null) {
            mRequestQueue = Volley.newRequestQueue(getApplicationContext());
        }
        return mRequestQueue;
    }

    public void cancelPendingRequests(Object tag) {
        if (mRequestQueue != null) {
            mRequestQueue.cancelAll(tag);
        }
    }


}
