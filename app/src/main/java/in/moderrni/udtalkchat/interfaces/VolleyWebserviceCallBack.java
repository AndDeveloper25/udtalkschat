package in.moderrni.udtalkchat.interfaces;

/**
 * Created by suheb on 24/5/17.
 */


import org.json.JSONObject;

public interface VolleyWebserviceCallBack {
    public void onSuccessApiCall(JSONObject serverResult, String requestTag, int statusCode);

    public void onErrorApiCall(JSONObject serverResult, String requestTag, int statusCode);
}
