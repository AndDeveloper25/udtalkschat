package in.moderrni.udtalkchat.interfaces;

import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Delete;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.Query;

import java.util.List;

import in.moderrni.udtalkchat.models.Group;
import in.moderrni.udtalkchat.models.User;


/*this interface will be use for basic operations on model*/
@Dao
public interface UserDao {


    //Status 0- delete user
    //status 1- active user
    // status 2- block user

    @Query("SELECT * FROM User WHERE status = :status")
    List<User> getAllUsers(int status);

    @Insert
    void insertAllUsers(List<User> userList);

    @Query("DELETE FROM User")
    void deleteAllUsers();


    @Query("UPDATE User SET status=0 WHERE user_id= :position")
    void deleteuser(int position);

    @Query("UPDATE User SET status=2 WHERE user_id= :blockposition")
    void blockuser(int blockposition);

    @Query("SELECT * FROM User WHERE status = :status AND profileFlag = :profileFlag")
    List<User> getOnlyContacts(int status, String profileFlag);


    @Query("SELECT * FROM User WHERE status = :status AND career = :usertype")
    List<User> getUserTypeList(boolean status, String usertype);


    @Query("SELECT * FROM User WHERE status = :status AND user_id = :userid")
    User getuserDetail(int status, int userid);


}
