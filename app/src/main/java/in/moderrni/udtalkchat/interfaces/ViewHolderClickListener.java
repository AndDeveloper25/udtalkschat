package in.moderrni.udtalkchat.interfaces;

public interface ViewHolderClickListener {
    public void setItemViewClick(int position);
}
