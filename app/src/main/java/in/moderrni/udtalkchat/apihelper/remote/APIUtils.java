package in.moderrni.udtalkchat.apihelper.remote;

public class APIUtils {

    private static final String BASE_URL = "";

    public static Webservices getRetrofitClient() {
        return RetrofitClient.getClient(BASE_URL).create(Webservices.class);
    }
}
